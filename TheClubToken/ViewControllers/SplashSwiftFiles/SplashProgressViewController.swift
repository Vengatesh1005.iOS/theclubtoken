//
//  SplashProgressViewController.swift
//  SpotnrideCustomer
//
//  Created by upltech on 10/07/19.
//  Copyright © 2019 Uplogic Technologies 1. All rights reserved.
//

import UIKit

class SplashProgressViewController: UIViewController {
   
    
    var timer: Timer?
    var i = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setupButtonView()
        // Do any additional setup after loading the view.
    }
    
    func setupButtonView() {
        
        i = 0.0
        
        timer = Timer.scheduledTimer(timeInterval: 0.03, target: self, selector: #selector(update), userInfo: nil, repeats: true)
    }
    
    @objc func update () {
        
        i += 1
        if i > 120 {
            timer?.invalidate()
            
            let islogin = UserDefaults.standard.bool(forKey: "user_isLogin")
            //
            if islogin
            {
               
                let forgotVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "UserLoginViewController") as? UserLoginViewController
                self.navigationController?.pushViewController(forgotVC!, animated: true)
                
            }
            else
            {
                let forgotVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "WelcomeViewController") as? WelcomeViewController
                self.navigationController?.pushViewController(forgotVC!, animated: true)
            
        }
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
}
