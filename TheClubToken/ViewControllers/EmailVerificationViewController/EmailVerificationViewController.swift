//
//  EmailVerificationViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 26/04/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class EmailVerificationViewController: UIViewController {

    @IBOutlet weak var bottomview: UIView!
    @IBOutlet weak var emailTxtffld: HoshiTextField!
    @IBOutlet weak var verificationTxtfld: HoshiTextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        bottomview.clipsToBounds = true
        bottomview.layer.cornerRadius = 50

        if #available(iOS 11.0, *) {
            bottomview.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        setDesigns()

        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        
        if #available(iOS 11.0, *){
            
        }else{
           let rectShape = CAShapeLayer()
           rectShape.bounds = view.frame
           rectShape.position = view.center
           rectShape.path = UIBezierPath(roundedRect: view.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 50 , height: 50)).cgPath
           bottomview.layer.mask = rectShape
    }
    
    }
    
    func setDesigns(){
        
        
        self.emailTxtffld.borderActiveColor = .clear
        self.emailTxtffld.borderInactiveColor = .clear
        self.emailTxtffld.placeholderColor = .textcolor
        self.emailTxtffld.textColor = .textcolor
        self.emailTxtffld.delegate = self
        
        self.verificationTxtfld.borderActiveColor = .clear
        self.verificationTxtfld.borderInactiveColor = .clear
        self.verificationTxtfld.placeholderColor = .textcolor
        self.verificationTxtfld.textColor = .textcolor
        self.verificationTxtfld.delegate = self

       
    }
   
    
    @IBAction func backact(_ sender: Any) {
        
        self.popLeft()
    }
    
    @IBAction func sendverificationcodeact(_ sender: Any) {
    }
   
    @IBAction func verifycodeact(_ sender: Any) {
        
        pinfrom = "register"
        self.push(id: "PINCodeSetViewController", animation: true, fromSB: Login)

        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension EmailVerificationViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == emailTxtffld
        {
        emailTxtffld.placeholder = "Enter Email Id"
        }
        
        else if textField == verificationTxtfld
        {
        verificationTxtfld.placeholder = "Enter Verification Code"
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count == 0 {
            
            
            if textField == emailTxtffld
                   {
                   emailTxtffld.placeholder = "Enter Email Id"
                    self.emailTxtffld.borderInactiveColor = .clear

                   }
                   
                   else if textField == verificationTxtfld
                   {
                   verificationTxtfld.placeholder = "Enter Verification Code"
                    self.verificationTxtfld.borderInactiveColor = .clear

                   }
                   

        }
    }
    
}
