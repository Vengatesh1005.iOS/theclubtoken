//
//  ProfileHelpViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 05/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class ProfileHelpViewController: UIViewController {

    @IBOutlet weak var accountline: UIView!
    @IBOutlet weak var accountview: UIView!
    @IBOutlet weak var authenticationview: UIView!
    @IBOutlet weak var authenticationline: UIView!
    @IBOutlet weak var helpview: UIView!
    @IBOutlet weak var helpline: UIView!

    @IBOutlet weak var accountbtn: UIButton!
    
    @IBOutlet weak var authbtn: UIButton!
    @IBOutlet weak var helpbtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func backbtn(_ sender: Any) {

        self.pushwithbackanimation(id: "UserProfileListViewController", animation: false, fromSB: Profile)

    }

    
    @IBAction func accountact(_ sender: Any) {
        
        self.push(id: "EditProfileViewController", animation: false, fromSB: Profile)
    }
    @IBAction func authenticationact(_ sender: Any) {
        self.push(id: "EditProfileAuthenticationViewController", animation: false, fromSB: Profile)
    }
    
    @IBAction func helpact(_ sender: Any) {
        
    }


    @IBAction func supportact(_ sender: Any) {
    }
    @IBAction func feedbackact(_ sender: Any) {
    }
   
    @IBAction func faqsact(_ sender: Any) {
    }
    
    @IBAction func termsandconditions(_ sender: Any) {
    }
    
    
    @IBAction func privacypolicy(_ sender: Any) {
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
