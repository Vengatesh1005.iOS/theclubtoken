//
//  EditProfileViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 05/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {

    @IBOutlet weak var accountline: UIView!
    @IBOutlet weak var accountview: UIView!
    @IBOutlet weak var authenticationview: UIView!
    @IBOutlet weak var authenticationline: UIView!
    @IBOutlet weak var helpview: UIView!
    @IBOutlet weak var helpline: UIView!
    @IBOutlet weak var firstnameTxtFld: HoshiTextField!
    @IBOutlet weak var middlenameTxtFld: HoshiTextField!
    @IBOutlet weak var lastnameTxtFld: HoshiTextField!
    @IBOutlet weak var phonenumberTxtFld: HoshiTextField!
    @IBOutlet weak var emailTxtFld: HoshiTextField!
    @IBOutlet weak var accountbtn: UIButton!
    
    @IBOutlet weak var authbtn: UIButton!
    @IBOutlet weak var helpbtn: UIButton!
    
    
    
    override func viewDidLoad() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("popleft"), object: nil)


       setDesigns()
    }

   
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        
        self.popLeft()
    }

    
    
    func setDesigns(){
        
        self.firstnameTxtFld.borderActiveColor = .textcolor
        self.firstnameTxtFld.borderInactiveColor = .buttonsecondary
        self.firstnameTxtFld.placeholderColor = .textcolor
        self.firstnameTxtFld.textColor = .textcolor
        self.firstnameTxtFld.delegate = self
        
        self.middlenameTxtFld.borderActiveColor = .textcolor
        self.middlenameTxtFld.borderInactiveColor = .buttonsecondary
        self.middlenameTxtFld.placeholderColor = .textcolor
        self.middlenameTxtFld.textColor = .textcolor
        self.middlenameTxtFld.delegate = self
       
        self.lastnameTxtFld.borderActiveColor = .textcolor
        self.lastnameTxtFld.borderInactiveColor = .buttonsecondary
        self.lastnameTxtFld.placeholderColor = .textcolor
        self.lastnameTxtFld.textColor = .textcolor
        self.lastnameTxtFld.delegate = self
        
        self.phonenumberTxtFld.borderActiveColor = .textcolor
        self.phonenumberTxtFld.borderInactiveColor = .buttonsecondary
        self.phonenumberTxtFld.placeholderColor = .textcolor
        self.phonenumberTxtFld.textColor = .textcolor
        self.phonenumberTxtFld.delegate = self

        self.emailTxtFld.borderActiveColor = .textcolor
        self.emailTxtFld.borderInactiveColor = .buttonsecondary
        self.emailTxtFld.placeholderColor = .textcolor
        self.emailTxtFld.textColor = .textcolor
        self.emailTxtFld.delegate = self
        
        
    }
    @IBAction func backbtn(_ sender: Any) {
        
        self.pushwithbackanimation(id: "UserProfileListViewController", animation: false, fromSB: Profile)

    }
    
    @IBAction func accountact(_ sender: Any) {
        
    }
    @IBAction func authenticationact(_ sender: Any) {
        
        self.push(id: "EditProfileAuthenticationViewController", animation: false, fromSB: Profile)
    }
    
    @IBAction func helpact(_ sender: Any) {
       

        self.push(id: "ProfileHelpViewController", animation: false, fromSB: Profile)
    }
    
    
    @IBAction func editact(_ sender: Any) {
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditProfileViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == firstnameTxtFld
        {
        firstnameTxtFld.placeholder = "First Name"
        }
        
        else if textField == lastnameTxtFld
        {
        lastnameTxtFld.placeholder = "Last Name"
        }
        else if textField == middlenameTxtFld
        {
        middlenameTxtFld.placeholder = "Middle Name"
        }
        else if textField == phonenumberTxtFld
        {
        phonenumberTxtFld.placeholder = "Phone Number"
        }
        else if textField == emailTxtFld
        {
        emailTxtFld.placeholder = "Email"
        }
        


    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField.text?.count == 0 {
            
            
            
            if textField == firstnameTxtFld
            {
            firstnameTxtFld.placeholder = "First Name"
                self.firstnameTxtFld.borderInactiveColor = .buttonsecondary

            }
            
            else if textField == lastnameTxtFld
            {
            lastnameTxtFld.placeholder = "Last Name"
                self.lastnameTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == middlenameTxtFld
            {
            middlenameTxtFld.placeholder = "Middle Name"
                self.middlenameTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == phonenumberTxtFld
            {
            phonenumberTxtFld.placeholder = "Phone Number"
                self.phonenumberTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == emailTxtFld
            {
            emailTxtFld.placeholder = "Email"
                self.emailTxtFld.borderInactiveColor = .buttonsecondary

            }
            
            
            
            
          
            
        }
    }
    
}
