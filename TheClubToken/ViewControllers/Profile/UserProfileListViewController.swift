//
//  UserProfileListViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 09/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class UserProfileListViewController: UIViewController {

    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var memberLbl: UILabel!
    @IBOutlet weak var packageLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backbtn(_ sender: Any) {
        
        
        let vc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainTabbarViewController") as? MainTabbarViewController
        self.navigationController?.pushViewController(vc!, animated: false)
      

        

    }
    @IBAction func editprofile(_ sender: Any) {
        
        self.push(id: "EditProfileViewController", animation: true, fromSB: Profile)
    }
    @IBAction func membership(_ sender: Any) {
    }
    
    @IBAction func registernewsubacccount(_ sender: Any) {
    }
    @IBAction func transfersubaccount(_ sender: Any) {
    }
    @IBAction func viewcertificate(_ sender: Any) {
    }
    @IBAction func submitkyc(_ sender: Any) {
        
        self.push(id: "KYCViewController1", animation: true, fromSB: Profile)
    }
    @IBAction func switchaccount(_ sender: Any) {
    }
    @IBAction func logout(_ sender: Any) {
        
        logoutConfirmation()
    }
    
    func logoutConfirmation(){
        let controller = UIAlertController(title: nil, message: "Are you sure want to logout?", preferredStyle: .actionSheet)
        let yes = UIAlertAction(title: "Yes", style: .default) { (action) in

            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            
            self.push(id: "WelcomeViewController", animation: true, fromSB: Login)
        }
        let no = UIAlertAction(title: "No", style: .cancel) { (action) in
        }
        controller.addAction(yes)
        controller.addAction(no)
        present(controller, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
