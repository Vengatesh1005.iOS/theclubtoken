//
//  EditProfileAuthenticationViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 05/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class EditProfileAuthenticationViewController: UIViewController {

    @IBOutlet weak var accountline: UIView!
    @IBOutlet weak var accountview: UIView!
    @IBOutlet weak var authenticationview: UIView!
    @IBOutlet weak var authenticationline: UIView!
    @IBOutlet weak var helpview: UIView!
    @IBOutlet weak var helpline: UIView!
    @IBOutlet weak var bioauthenticationswitch: UISwitch!
    @IBOutlet weak var enablepinTransferswitch: UISwitch!
   
    @IBOutlet weak var accountbtn: UIButton!
    
    @IBOutlet weak var authbtn: UIButton!
    @IBOutlet weak var helpbtn: UIButton!

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switchreSize()
        // Do any additional setup after loading the view.
    }
    
    func switchreSize()
        
    {
        bioauthenticationswitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        enablepinTransferswitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
    }
    
    @IBAction func backbtn(_ sender: Any) {

      //  self.popLeftWithoutAnimation()
        self.pushwithbackanimation(id: "UserProfileListViewController", animation: false, fromSB: Profile)
        //NotificationCenter.default.post(name: Notification.Name("popleft"), object: nil)
    }

    
       @IBAction func accountact(_ sender: Any) {
        
        self.push(id: "EditProfileViewController", animation: false, fromSB: Profile)
    }
    @IBAction func authenticationact(_ sender: Any) {
    }
    
    @IBAction func helpact(_ sender: Any) {
        
        self.push(id: "ProfileHelpViewController", animation: false, fromSB: Profile)
    }


    @IBAction func changepinact(_ sender: Any) {
    }
     @IBAction func changesecondarypin(_ sender: Any) {
     }
     
    @IBAction func autolockact(_ sender: Any) {
    }
    
    @IBAction func bioauthenticationact(_ sender: Any) {
    }
    
    @IBAction func enablepintransferact(_ sender: Any) {
    }
    /*
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
