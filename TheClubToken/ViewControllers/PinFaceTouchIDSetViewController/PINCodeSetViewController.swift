//
//  PINCodeSetViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 27/04/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
class PINCodeSetViewController: UIViewController {

    @IBOutlet weak var setbtnoutlet: UIButton!
    
    @IBOutlet var pinselectionview: [UIView]!
    @IBOutlet var pintypebuttons: [UIButton]!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    @IBOutlet weak var activeinactivebtn: UIButton!
    @IBOutlet weak var passcodeview: UIView!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var backbtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //create PasswordContainerView
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if pinfrom == "register"
        {
            self.setbtnoutlet.setTitle("SET", for: .normal)
            self.backImg.isHidden = true
            self.backbtn.isHidden = true
        }
        else
        {
            self.setbtnoutlet.setTitle("LOG IN", for: .normal)
            self.backImg.isHidden = false
            self.backbtn.isHidden = false

        }
        
        lbl1.text = ""
        lbl2.text = ""
        lbbl3.text = ""
        lbl4.text = ""
        lbl5.text = ""
        lbl6.text = ""
        enteredpin = ""
        colorupdate(from: "delete")
        
    }
    
    func colorupdate(from:String)
    {
        if from == "delete"
        {
            if lbl6.text == ""
            {
                lbl6.backgroundColor = UIColor.clear
            }
            
              if lbl5.text == ""
            {
                lbl5.backgroundColor = UIColor.clear

            }
              if lbl4.text == ""
            {
                lbl4.backgroundColor = UIColor.clear

            }
              if lbbl3.text == ""
            {
                lbbl3.backgroundColor = UIColor.clear

            }
              if lbl2.text == ""
            {
                lbl2.backgroundColor = UIColor.clear

            }
              if lbl1.text == ""
            {
                lbl1.backgroundColor = UIColor.clear

            }
            buttoncolor()

            
        }
        else
        {
           
            if lbl1.text != ""
            {
                lbl1.backgroundColor = UIColor.buttonprimary
            }
            
            if lbl2.text != ""
            {
                lbl2.backgroundColor = UIColor.buttonprimary

            }
             if lbbl3.text != ""
            {
                lbbl3.backgroundColor = UIColor.buttonprimary

            }
             if lbl4.text != ""
            {
                lbl4.backgroundColor = UIColor.buttonprimary

            }
             if lbl5.text != ""
            {
                lbl5.backgroundColor = UIColor.buttonprimary

            }
             if lbl6.text != ""
            {
                lbl6.backgroundColor = UIColor.buttonprimary

            }
            buttoncolor()

        }
    }
    
    
    
    @IBAction func numberbtnact(_ sender: UIButton) {
        
        UIView .animate(withDuration: 0.1, animations: {
            sender.backgroundColor = UIColor.buttonprimary
        }, completion: { completed in
            if completed {
                sender.backgroundColor = UIColor.buttonprimary.withAlphaComponent(0.2)
            }
        })
        
        if lbl1.text == ""
        {

            lbl1.text = "\(sender.tag)"
            colorupdate(from: "add")
            
        }
        
        else if lbl2.text == ""
        {
            lbl2.text = "\(sender.tag)"
            colorupdate(from: "add")

        }
        else if lbbl3.text == ""
        {
            lbbl3.text = "\(sender.tag)"
            colorupdate(from: "add")

        }
        else if lbl4.text == ""
        {
            lbl4.text = "\(sender.tag)"
            colorupdate(from: "add")

        }
        else if lbl5.text == ""
        {
            lbl5.text = "\(sender.tag)"
            colorupdate(from: "add")

        }
        else if lbl6.text == ""
        {
            lbl6.text = "\(sender.tag)"
            colorupdate(from: "add")

        }
        
        
    }
    
    @IBAction func acivatebtnblink(_ sender: Any) {
        
        passcodeview.shake()

    }
    @IBAction func clearbtnact(_ sender: UIButton) {
     
        UIView .animate(withDuration: 0.1, animations: {
            sender.backgroundColor = UIColor.buttonprimary
        }, completion: { completed in
            if completed {
                sender.backgroundColor = UIColor.clear
                
            }
        })
        
        
        if lbl6.text != ""
        {
            lbl6.text = ""
        }
        
         else if lbl5.text != ""
        {
            lbl5.text = ""

        }
         else if lbl4.text != ""
        {
            lbl4.text = ""

        }
        else if lbbl3.text != ""
        {
            lbbl3.text = ""

        }
         else if lbl2.text != ""
        {
            lbl2.text = ""

        }
         else if lbl1.text != ""
        {
            lbl1.text = ""

        }
        
        colorupdate(from: "delete")
    }
    
    
    func buttoncolor()
    {
        
        if lbl1.text == "" || lbl2.text == "" || lbbl3.text == "" || lbl4.text == "" || lbl5.text == "" || lbl6.text == ""
        {
            activeinactivebtn.isHidden = false
        }
        else
        {
            activeinactivebtn.isHidden = true
        }
        
        
    }
    @IBAction func setact(_ sender: Any) {
     
        
        let c = lbl1.text! + lbl2.text! + lbbl3.text!
        let c1 = c + lbl4.text! + lbl5.text! + lbl6.text!

        print("Selected Pin \(c1)")

enteredpin = c1
        
        
if pinfrom == "register"
{
    
    
    self.push(id: "ConfirmPinViewController", animation: true, fromSB: Login)
    
    
    
//    UserDefaults.standard.set(c1, forKey: "pin")
//
//        self.showAlertView(title: appNameShort, message: "Pin Successfully Set") { (true) in
//            self.push(id: "TouchIDSetViewController", animation: true, fromSB: Login)
//        }
        }
        
        
        else
{
    if let f_id = UserDefaults.standard.string(forKey: "pin")

    {
        if f_id == c1
        {
            let vc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainTabbarViewController") as? MainTabbarViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        else
        {
            passcodeview.shake()

        }
        
    }
    
        }
        
    }
    
    @IBAction func backact(_ sender: Any) {
        self.popLeft()
    }
    
    
}
extension UIView {
    func blink(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, alpha: CGFloat = 0.0) {
        UIView.animate(withDuration: duration, delay: delay, options: [.curveEaseInOut, .repeat, .autoreverse], animations: {
            self.alpha = alpha
        })
    }
}



