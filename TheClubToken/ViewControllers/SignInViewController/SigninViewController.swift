//
//  SigninViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 26/04/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class SigninViewController: UIViewController {

    @IBOutlet weak var emailTxtffld: HoshiTextField!
    @IBOutlet weak var verificationTxtfld: HoshiTextField!
    @IBOutlet weak var signupLbl: UILabel!

    @IBOutlet weak var bottomview: UIView!

    var myString = "Don't have an account?Signup now"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomview.clipsToBounds = true
        bottomview.layer.cornerRadius = 50

        if #available(iOS 11.0, *) {
            bottomview.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        setDesigns()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        
        if #available(iOS 11.0, *){
            
        }else{
           let rectShape = CAShapeLayer()
           rectShape.bounds = view.frame
           rectShape.position = view.center
           rectShape.path = UIBezierPath(roundedRect: view.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 50 , height: 50)).cgPath
           bottomview.layer.mask = rectShape
    }
    
    }
    
    
    func setDesigns(){
        
        self.emailTxtffld.borderActiveColor = .textcolor
        self.emailTxtffld.borderInactiveColor = .lightGray
        self.emailTxtffld.placeholderColor = .textcolor
        self.emailTxtffld.textColor = .textcolor
        self.emailTxtffld.delegate = self
        
        self.verificationTxtfld.borderActiveColor = .textcolor
        self.verificationTxtfld.borderInactiveColor = .lightGray
        self.verificationTxtfld.placeholderColor = .textcolor
        self.verificationTxtfld.textColor = .textcolor
        self.verificationTxtfld.delegate = self
       
        let myMutableString = NSMutableAttributedString(string: myString, attributes: [NSAttributedString.Key.font :UIFont(name: "Montserrat-Regular", size: 17.0)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.buttonprimary, range: NSRange(location:22,length:10))
        signupLbl.isUserInteractionEnabled = true

        signupLbl.attributedText = myMutableString
        signupLbl.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))

    }
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
       let termsRange = (myString as NSString).range(of: "Signup now")
       // comment for now
       //let privacyRange = (text as NSString).range(of: "Privacy Policy")

       if gesture.didTapAttributedTextInLabel(label: signupLbl, inRange: termsRange) {

        self.push(id: "RegisterViewController", animation: true, fromSB: Login)
       }  else {
           print("Tapped none")
       }
    }

  
    @IBAction func backact(_ sender: Any) {
        
        self.popLeft()
        
    }
    @IBAction func signinact(_ sender: Any) {
        
        self.push(id: "MobileVerificationViewController", animation: true, fromSB: Login)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SigninViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == emailTxtffld
        {
        emailTxtffld.placeholder = "Your Email*"
        }
        
        else if textField == verificationTxtfld
        {
        verificationTxtfld.placeholder = "Enter Verification Code*"
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count == 0 {
            
            
            if textField == emailTxtffld
                   {
                   emailTxtffld.placeholder = "Your Email*"
                    self.emailTxtffld.borderInactiveColor = .lightGray

                   }
                   
                   else if textField == verificationTxtfld
                   {
                   verificationTxtfld.placeholder = "Enter Verification Code*"
                    self.verificationTxtfld.borderInactiveColor = .lightGray

                   }
                   

        }
    }
    
}
extension UITapGestureRecognizer {

    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                              //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)

        //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
                                                        // locationOfTouchInLabel.y - textContainerOffset.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }

}
