//
//  HomeViewController.swift
//  TheClubToken
//
//  Created by trioangle on 05/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import ExpandableCell

class TabHomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var DoneBtn: UIButton!
    @IBOutlet var tableView: ExpandableTableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
        var parentCells: [[String]] = [
            [ExpandableCell1.ID,
             ExpandableCell1.ID
            ]
        ]
    
        var cell: UITableViewCell {
            return tableView.dequeueReusableCell(withIdentifier: ExpandedCell.ID)!
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()


            collectionView.delegate = self
            collectionView.dataSource = self
            
            let cellWidth : CGFloat = collectionView.frame.size.width
            let cellheight : CGFloat = collectionView.frame.size.height
            let cellSize = CGSize(width: cellWidth , height:cellheight)

            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.itemSize = cellSize
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            collectionView.setCollectionViewLayout(layout, animated: true)
            
            collectionView.reloadData()
            
            tableView.expandableDelegate = self
            tableView.animation = .automatic
            
            
                tableView.register(UINib(nibName: "ExpandedCell", bundle: nil), forCellReuseIdentifier: ExpandedCell.ID)
                
                tableView.register(UINib(nibName: "ExpandableCell", bundle: nil), forCellReuseIdentifier: ExpandableCell1.ID)
                
            }
    
            override func viewDidAppear(_ animated: Bool) {
                super.viewDidAppear(animated)
                
        //        tableView.openAll()
            }

    @IBAction func profileact(_ sender: Any) {
        
        self.push(id: "UserProfileListViewController", animation: true, fromSB: Profile)
    }
    
    
    override func didReceiveMemoryWarning() {
                super.didReceiveMemoryWarning()
                // Dispose of any resources that can be recreated.
            }
            
            @IBAction func openAllButtonClicked() {
                tableView.openAll()
            }
            
            @IBAction func expandMultiButtonClicked(_ sender: Any) {
                tableView.expansionStyle = .multi
            }
            
            @IBAction func expandSingleButtonClicked(_ sender: Any) {
                tableView.expansionStyle = .single
                tableView.closeAll()
            }
            
            @IBAction func closeAllButtonClicked() {
                tableView.closeAll()
            }
            
            @IBAction func SelectionDisplayOn(_ sender: UIButton) {
                tableView.autoRemoveSelection = !tableView.autoRemoveSelection
                let isOn = tableView.autoRemoveSelection ? "Off" : "On"
                sender.setTitle("Selection Stays \(isOn)", for: .normal)
            }
            
            //scroll view methods are being forwarded correctly
            func scrollViewDidScroll(_ scrollView: UIScrollView) {
                print("scrollViewDidScroll")
            }
            
            func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
                print("scrollViewDidScroll, decelerate:\(decelerate)")
            }
            func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
                print("scrollViewDidEndScrollingAnimation")
            }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return 1
      }

      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
          cell.backgroundColor = .red
          return cell
      }
        }

        extension TabHomeViewController: ExpandableDelegate {
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
                switch indexPath.section {
                case 0:
                    switch indexPath.row {
                    case 0:
                        print(indexPath.row)
                        let cell1 = tableView.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                       // cell1.titleLabel.text = "First Expanded Cell"
                        let cell2 = tableView.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                       // cell2.titleLabel.text = "Second Expanded Cell"
                        let cell3 = tableView.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                        
                        cell3.baseView.roundCorners(corners: [.bottomLeft, .
                        bottomRight], radius: 10.0)
                       // cell3.titleLabel.text = "Third Expanded Cell"
                        return [cell1, cell2, cell3]
                        
                        case 1:
                         let cell1 = tableView.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                        // cell1.titleLabel.text = "First Expanded Cell"
                         let cell2 = tableView.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                        // cell2.titleLabel.text = "Second Expanded Cell"
                         cell2.baseView.roundCorners(corners: [.bottomLeft, .
                         bottomRight], radius: 10.0)
                         return [cell1, cell2]
                        
                    case 2:
                        return [cell, cell]
                    case 3:
                        return [cell, cell, cell]
                    case 5:
                        return [cell, cell, cell]
                    default:
                        break
                    }
                default:
                    break
                }
                return nil
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
                switch indexPath.section {
                case 0:
                    switch indexPath.row {
                    case 0:
                        return [90, 90, 90]
                        
                        case 1:
                        return [90, 90]
                        
                    case 2:
                        return [33, 33, 33]
                        
                    case 3:
                        return [22]
                    case 5:
                        return [33, 33, 33]
                    default:
                        break
                    }

                default:
                    break
                }
                return nil
                
            }
            
            func numberOfSections(in tableView: ExpandableTableView) -> Int {
                return parentCells.count
            }

            func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
                return parentCells[section].count
            }

            func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAt indexPath: IndexPath) {
//                let cell = expandableTableView.dequeueReusableCell(withIdentifier: parentCells[indexPath.section][indexPath.row]) as! ExpandableCell1
//
//                cell.baseView.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
                print("didSelectRow:\(indexPath)")
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectExpandedRowAt indexPath: IndexPath) {
                print("didSelectExpandedRowAt:\(indexPath)")
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCell: UITableViewCell, didSelectExpandedRowAt indexPath: IndexPath) {
                if let cell = expandedCell as? ExpandedCell {
                    print("\(cell.titleLabel.text ?? "")")
                }
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, titleForHeaderInSection section: Int) -> String? {
                return "Section:\(section)"
            }
            func expandableTableView(_ expandableTableView: ExpandableTableView, heightForHeaderInSection section: Int) -> CGFloat {
                return 0
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = expandableTableView.dequeueReusableCell(withIdentifier: parentCells[indexPath.section][indexPath.row]) as! ExpandableCell1
                
                cell.baseView.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
//                cell.baseView.roundCorners(corners: [.bottomLeft
//                    , .bottomRight], radius: 10.0)
                
                //cell.
                return cell
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                switch indexPath.section {
                case 0:
                    switch indexPath.row {
                    case 0, 2, 3:
                        return 120
                        
                    case 1, 4:
                        return 120
                        
                    default:
                        break
                    }
     
                default:
                    break
                }
                
                return 44
            }
            
            @objc(expandableTableView:didCloseRowAt:) func expandableTableView(_ expandableTableView: UITableView, didCloseRowAt indexPath: IndexPath) {
                let cell = expandableTableView.cellForRow(at: indexPath)
                cell?.contentView.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
                cell?.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
            }
            
            func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
                return true
            }
            
            func expandableTableView(_ expandableTableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        //        let cell = expandableTableView.cellForRow(at: indexPath)
        //        cell?.contentView.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        //        cell?.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            }
            
        //    func expandableTableView(_ expandableTableView: ExpandableTableView, titleForHeaderInSection section: Int) -> String? {
        //        return "Section \(section)"
        //    }
        //
        //    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForHeaderInSection section: Int) -> CGFloat {
        //        return 33
        //    }
        }





