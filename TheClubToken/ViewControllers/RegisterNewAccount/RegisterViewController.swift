//
//  RegisterViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 25/04/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    @IBOutlet weak var bottomview: UIView!

    @IBOutlet weak var nameTxtffld: HoshiTextField!
    @IBOutlet weak var middlenameTxtfld: HoshiTextField!
    @IBOutlet weak var lastnameTxtflfd: HoshiTextField!
    @IBOutlet weak var referalTxtfld: HoshiTextField!
    @IBOutlet weak var epinTxtfld: HoshiTextField!
    @IBOutlet weak var privacypoliceimg: UIImageView!
    @IBOutlet weak var termsLbl: UILabel!
    @IBOutlet weak var liveenvironmentImg: UIImageView!
    
    var privaypolicy = false
    var liveenv = false
    
    var myString = "I accept Privacy Policy and Terms of Conditions"

    
    override func viewDidLoad() {
        super.viewDidLoad()

        bottomview.clipsToBounds = true
        bottomview.layer.cornerRadius = 50
        if #available(iOS 11.0, *) {
            bottomview.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }

        setDesigns()


        // Do any additional setup after loading the view.
    }
    
    @IBAction func privacypolicyselect(_ sender: Any) {
        
        if(privaypolicy == false) {
            
            self.privacypoliceimg.image = #imageLiteral(resourceName: "tick")

        } else {
            
            self.privacypoliceimg.image = #imageLiteral(resourceName: "circle")
        }
        
        privaypolicy = !privaypolicy
    }
    
    @IBAction func liveenvironmentact(_ sender: Any) {
        
        if(liveenv == false) {
            
            self.liveenvironmentImg.image = #imageLiteral(resourceName: "tick")

        } else {
            
            self.liveenvironmentImg.image = #imageLiteral(resourceName: "circle")
        }
        
        liveenv = !liveenv
    }
    
    @IBAction func backact(_ sender: Any) {
        
        self.popLeft()
        
    }
    func setDesigns(){
        
        self.nameTxtffld.borderActiveColor = .textcolor
        self.nameTxtffld.borderInactiveColor = .lightGray
        self.nameTxtffld.placeholderColor = .textcolor
        self.nameTxtffld.textColor = .textcolor
        self.nameTxtffld.delegate = self
        
        self.middlenameTxtfld.borderActiveColor = .textcolor
        self.middlenameTxtfld.borderInactiveColor = .lightGray
        self.middlenameTxtfld.placeholderColor = .textcolor
        self.middlenameTxtfld.textColor = .textcolor
        self.middlenameTxtfld.delegate = self
       
        self.lastnameTxtflfd.borderActiveColor = .textcolor
        self.lastnameTxtflfd.borderInactiveColor = .lightGray
        self.lastnameTxtflfd.placeholderColor = .textcolor
        self.lastnameTxtflfd.textColor = .textcolor
        self.lastnameTxtflfd.delegate = self
        
        self.referalTxtfld.borderActiveColor = .textcolor
               self.referalTxtfld.borderInactiveColor = .lightGray
               self.referalTxtfld.placeholderColor = .textcolor
               self.referalTxtfld.textColor = .textcolor
               self.referalTxtfld.delegate = self
        
        self.epinTxtfld.borderActiveColor = .textcolor
               self.epinTxtfld.borderInactiveColor = .lightGray
               self.epinTxtfld.placeholderColor = .textcolor
               self.epinTxtfld.textColor = .textcolor
               self.epinTxtfld.delegate = self

        
        let myMutableString = NSMutableAttributedString(string: myString, attributes: [NSAttributedString.Key.font :UIFont(name: "Montserrat-Regular", size: 12.0)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.buttonprimary, range: NSRange(location:9,length:15))
        
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.buttonprimary, range: NSRange(location:28,length:19))

        termsLbl.isUserInteractionEnabled = true

        termsLbl.attributedText = myMutableString
        termsLbl.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))

        
    }
    
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
       let termsRange = (myString as NSString).range(of: "Privacy Policy")
       // comment for now
       let privacyRange = (myString as NSString).range(of: "Terms of Conditions")

       if gesture.didTapAttributedTextInLabel(label: termsLbl, inRange: termsRange) {
        
        self.view.make(toast: "Privacy Policy")

       }
             else if gesture.didTapAttributedTextInLabel(label: termsLbl, inRange: privacyRange)
              {
        
                self.view.make(toast: "Terms of Conditions")

        }
       
       else {
           print("Tapped none")
       }
    }
    
    override func viewDidLayoutSubviews() {
        if #available(iOS 11.0, *){
            
        }else{
           let rectShape = CAShapeLayer()
           rectShape.bounds = view.frame
           rectShape.position = view.center
           rectShape.path = UIBezierPath(roundedRect: view.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 50 , height: 50)).cgPath
           bottomview.layer.mask = rectShape
    }
    
    }
    
    
    @IBAction func continueact(_ sender: Any) {
        
        self.push(id: "MobileVerificationViewController", animation: true, fromSB: Login)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RegisterViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == nameTxtffld
        {
        nameTxtffld.placeholder = "Name*"
        }
        
        else if textField == middlenameTxtfld
        {
        middlenameTxtfld.placeholder = "Middle Name*"
        }
        else if textField == lastnameTxtflfd
        {
        lastnameTxtflfd.placeholder = "Last Name*"
        }
        else if textField == referalTxtfld
        {
        referalTxtfld.placeholder = "Referral Code*"
        }
        else if textField == epinTxtfld
        {
        epinTxtfld.placeholder = "EPIN (Optional)"
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count == 0 {
            
            
            if textField == nameTxtffld
                   {
                   nameTxtffld.placeholder = "Name*"
                    self.nameTxtffld.borderInactiveColor = .lightGray

                   }
                   
                   else if textField == middlenameTxtfld
                   {
                   middlenameTxtfld.placeholder = "Middle Name*"
                    self.middlenameTxtfld.borderInactiveColor = .lightGray

                   }
                   else if textField == lastnameTxtflfd
                   {
                   lastnameTxtflfd.placeholder = "Last Name*"
                    self.lastnameTxtflfd.borderInactiveColor = .lightGray

                   }
                   else if textField == referalTxtfld
                   {
                   referalTxtfld.placeholder = "Referral Code*"
                    self.referalTxtfld.borderInactiveColor = .lightGray

                   }
            
            else if textField == epinTxtfld
            {
            epinTxtfld.placeholder = "EPIN (Optional)"
             self.epinTxtfld.borderInactiveColor = .lightGray

            }

        }
    }
    
}
