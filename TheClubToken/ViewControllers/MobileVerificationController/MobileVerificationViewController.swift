//
//  MobileVerificationViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 26/04/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CountryPickerView
class MobileVerificationViewController: UIViewController {

    @IBOutlet weak var bottomview: UIView!
    @IBOutlet weak var mobileTxtffld: HoshiTextField!
    @IBOutlet weak var verificationTxtfld: HoshiTextField!

    @IBOutlet weak var countryImg: UIImageView!
    @IBOutlet weak var countryCodeLbl: UILabel!
    
    let countryPickerView = CountryPickerView()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        bottomview.clipsToBounds = true
        bottomview.layer.cornerRadius = 50

        if #available(iOS 11.0, *) {
            bottomview.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        setDesigns()

        
        countryPickerView.delegate = self
        countryPickerView.dataSource = self

        countryImg.image = #imageLiteral(resourceName: "india")
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        
        if #available(iOS 11.0, *){
            
        }else{
           let rectShape = CAShapeLayer()
           rectShape.bounds = view.frame
           rectShape.position = view.center
           rectShape.path = UIBezierPath(roundedRect: view.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 50 , height: 50)).cgPath
           bottomview.layer.mask = rectShape
    }
    
    }
    
    func setDesigns(){
        
        
        self.mobileTxtffld.borderActiveColor = .clear
        self.mobileTxtffld.borderInactiveColor = .clear
        self.mobileTxtffld.placeholderColor = .textcolor
        self.mobileTxtffld.textColor = .textcolor
        self.mobileTxtffld.delegate = self
        
        self.verificationTxtfld.borderActiveColor = .clear
        self.verificationTxtfld.borderInactiveColor = .clear
        self.verificationTxtfld.placeholderColor = .textcolor
        self.verificationTxtfld.textColor = .textcolor
        self.verificationTxtfld.delegate = self

       
    }
   
    @IBAction func backact(_ sender: Any) {
        
        self.popLeft()
    }
    
    
    @IBAction func countryBtnAct(_ sender: Any) {
        
        countryPickerView.showCountriesList(from: self)

    }
    @IBAction func sendverificationcodeact(_ sender: Any) {
       }
      
    @IBAction func verifycodeact(_ sender: Any) {
        
        
        self.push(id: "EmailVerificationViewController", animation: true, fromSB: Login)

        
       }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MobileVerificationViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == mobileTxtffld
        {
        mobileTxtffld.placeholder = "Enter Mobile Number"
        }
        
        else if textField == verificationTxtfld
        {
        verificationTxtfld.placeholder = "Enter Verification Code"
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count == 0 {
            
            
            if textField == mobileTxtffld
                   {
                   mobileTxtffld.placeholder = "Enter Mobile Number"
                    self.mobileTxtffld.borderInactiveColor = .clear

                   }
                   
                   else if textField == verificationTxtfld
                   {
                   verificationTxtfld.placeholder = "Enter Verification Code"
                    self.verificationTxtfld.borderInactiveColor = .clear

                   }
                   

        }
    }
    
}

extension MobileVerificationViewController: CountryPickerViewDelegate, CountryPickerViewDataSource {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        // Only countryPickerInternal has it's delegate set
        let message = "Name: \(country.name) \nCode: \(country.code) \nPhone: \(country.phoneCode)"
        
        print("Selected Country \(message)")
        self.countryCodeLbl.text = country.phoneCode
       self.countryImg.image = country.flag
    
    }
}
