//
//  KYCViewController1.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 03/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class KYCViewController1: UIViewController {

    @IBOutlet weak var firstnameTxtFld: HoshiTextField!
    @IBOutlet weak var middlenameTxtFld: HoshiTextField!
    @IBOutlet weak var lastnameTxtFld: HoshiTextField!
    @IBOutlet weak var genderTxtFld: HoshiTextField!
    @IBOutlet weak var dobTxtFld: HoshiTextField!
    @IBOutlet weak var emailTxtFld: HoshiTextField!
    @IBOutlet weak var phonenumberTxtFld: HoshiTextField!
    @IBOutlet weak var yourintensionTxtFld: HoshiTextField!
    @IBOutlet weak var downarrow: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        setDesigns()
        // Do any additional setup after loading the view.
    }
    
    
    func setDesigns(){
        
        self.firstnameTxtFld.borderActiveColor = .textcolor
        self.firstnameTxtFld.borderInactiveColor = .buttonsecondary
        self.firstnameTxtFld.placeholderColor = .textcolor
        self.firstnameTxtFld.textColor = .textcolor
        self.firstnameTxtFld.delegate = self
        
        self.middlenameTxtFld.borderActiveColor = .textcolor
        self.middlenameTxtFld.borderInactiveColor = .buttonsecondary
        self.middlenameTxtFld.placeholderColor = .textcolor
        self.middlenameTxtFld.textColor = .textcolor
        self.middlenameTxtFld.delegate = self
       
        self.lastnameTxtFld.borderActiveColor = .textcolor
        self.lastnameTxtFld.borderInactiveColor = .buttonsecondary
        self.lastnameTxtFld.placeholderColor = .textcolor
        self.lastnameTxtFld.textColor = .textcolor
        self.lastnameTxtFld.delegate = self
        
        self.genderTxtFld.borderActiveColor = .textcolor
        self.genderTxtFld.borderInactiveColor = .buttonsecondary
        self.genderTxtFld.placeholderColor = .textcolor
        self.genderTxtFld.textColor = .textcolor
        self.genderTxtFld.delegate = self

        self.dobTxtFld.borderActiveColor = .textcolor
        self.dobTxtFld.borderInactiveColor = .buttonsecondary
        self.dobTxtFld.placeholderColor = .textcolor
        self.dobTxtFld.textColor = .textcolor
        self.dobTxtFld.delegate = self
        
        self.emailTxtFld.borderActiveColor = .textcolor
        self.emailTxtFld.borderInactiveColor = .buttonsecondary
        self.emailTxtFld.placeholderColor = .textcolor
        self.emailTxtFld.textColor = .textcolor
        self.emailTxtFld.delegate = self
        
        self.phonenumberTxtFld.borderActiveColor = .textcolor
        self.phonenumberTxtFld.borderInactiveColor = .buttonsecondary
        self.phonenumberTxtFld.placeholderColor = .textcolor
        self.phonenumberTxtFld.textColor = .textcolor
        self.phonenumberTxtFld.delegate = self
        
        self.yourintensionTxtFld.borderActiveColor = .textcolor
        self.yourintensionTxtFld.borderInactiveColor = .buttonsecondary
        self.yourintensionTxtFld.placeholderColor = .textcolor
        self.yourintensionTxtFld.textColor = .textcolor
        self.yourintensionTxtFld.delegate = self
        downarrow.image = downarrow.image?.withRenderingMode(.alwaysTemplate)
        downarrow.tintColor = UIColor.buttonsecondary

        
    }
    @IBAction func backact(_ sender: Any) {
        
        self.popLeft()
    }
    
    @IBAction func genderact(_ sender: Any) {
    }
    @IBAction func dobact(_ sender: Any) {
    }
    @IBAction func previousact(_ sender: Any) {
        
        self.popLeft()

    }
    
    @IBAction func nextact(_ sender: Any) {
        
        self.push(id: "KYCViewController2", animation: false, fromSB: Profile)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension KYCViewController1 : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == firstnameTxtFld
        {
        firstnameTxtFld.placeholder = "First Name"
        }
        
        else if textField == middlenameTxtFld
        {
        middlenameTxtFld.placeholder = "Middle Name"
        }
        else if textField == lastnameTxtFld
        {
        lastnameTxtFld.placeholder = "Last Name"
        }
        else if textField == genderTxtFld
        {
        genderTxtFld.placeholder = "Gender"
        }
        else if textField == dobTxtFld
        {
        dobTxtFld.placeholder = "Date of Birth"
        }
        
        else if textField == emailTxtFld
        {
        emailTxtFld.placeholder = "Email"
        }

        else if textField == phonenumberTxtFld
        {
        phonenumberTxtFld.placeholder = "Phone Number"
        }

        else if textField == yourintensionTxtFld
        {
        yourintensionTxtFld.placeholder = "Your Intension for Purchase"
        }

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField.text?.count == 0 {
            
            
            
            
            if textField == firstnameTxtFld
            {
            firstnameTxtFld.placeholder = "First Name"
                self.firstnameTxtFld.borderInactiveColor = .buttonsecondary

                
            }
            
            else if textField == middlenameTxtFld
            {
            middlenameTxtFld.placeholder = "Middle Name"
                self.middlenameTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == lastnameTxtFld
            {
            lastnameTxtFld.placeholder = "Last Name"
                self.lastnameTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == genderTxtFld
            {
            genderTxtFld.placeholder = "Gender"
                self.genderTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == dobTxtFld
            {
            dobTxtFld.placeholder = "Date of Birth"
                self.dobTxtFld.borderInactiveColor = .buttonsecondary

            }
            
            else if textField == emailTxtFld
            {
            emailTxtFld.placeholder = "Email"
                self.emailTxtFld.borderInactiveColor = .buttonsecondary

            }

            else if textField == phonenumberTxtFld
            {
            phonenumberTxtFld.placeholder = "Phone Number"
                
                self.phonenumberTxtFld.borderInactiveColor = .buttonsecondary

            }

            else if textField == yourintensionTxtFld
            {
            yourintensionTxtFld.placeholder = "Your Intension for Purchase"
                self.yourintensionTxtFld.borderInactiveColor = .buttonsecondary

            }
            
        }
    }
    
}
