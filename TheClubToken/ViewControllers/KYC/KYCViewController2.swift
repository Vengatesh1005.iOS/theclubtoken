//
//  KYCViewController2.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 03/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class KYCViewController2: UIViewController {

    @IBOutlet weak var emptyTxtFld: HoshiTextField!
    @IBOutlet weak var residentialAddrLbl: UILabel!
    @IBOutlet weak var addressTxtView: UITextView!
    @IBOutlet weak var citytownTxtfFld: HoshiTextField!
    @IBOutlet weak var postalcodeTxtFld: HoshiTextField!
    @IBOutlet weak var downarrow1: UIImageView!
    @IBOutlet weak var downarrow2: UIImageView!
    @IBOutlet weak var nationalityTxtFld: HoshiTextField!
    @IBOutlet weak var countryTxtFld: HoshiTextField!
    @IBOutlet weak var downarrow3: UIImageView!
    @IBOutlet weak var regionstateTxtFld: HoshiTextField!
    @IBOutlet weak var downarrow4: UIImageView!
    @IBOutlet weak var bottomview: UIView!
    @IBOutlet weak var bottomviewheight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        setDesigns()
        // Do any additional setup after loading the view.
    }
    
    
    func setDesigns(){
        
        self.emptyTxtFld.borderActiveColor = .clear
        self.emptyTxtFld.borderInactiveColor = .clear
        self.emptyTxtFld.placeholderColor = .clear
        self.emptyTxtFld.textColor = .clear
        self.emptyTxtFld.delegate = self
        
        self.citytownTxtfFld.borderActiveColor = .textcolor
        self.citytownTxtfFld.borderInactiveColor = .buttonsecondary
        self.citytownTxtfFld.placeholderColor = .textcolor
        self.citytownTxtfFld.textColor = .textcolor
        self.citytownTxtfFld.delegate = self
       
        self.postalcodeTxtFld.borderActiveColor = .textcolor
        self.postalcodeTxtFld.borderInactiveColor = .buttonsecondary
        self.postalcodeTxtFld.placeholderColor = .textcolor
        self.postalcodeTxtFld.textColor = .textcolor
        self.postalcodeTxtFld.delegate = self
        
        self.nationalityTxtFld.borderActiveColor = .textcolor
        self.nationalityTxtFld.borderInactiveColor = .buttonsecondary
        self.nationalityTxtFld.placeholderColor = .textcolor
        self.nationalityTxtFld.textColor = .textcolor
        self.nationalityTxtFld.delegate = self

        self.countryTxtFld.borderActiveColor = .textcolor
        self.countryTxtFld.borderInactiveColor = .buttonsecondary
        self.countryTxtFld.placeholderColor = .textcolor
        self.countryTxtFld.textColor = .textcolor
        self.countryTxtFld.delegate = self
        
        self.regionstateTxtFld.borderActiveColor = .textcolor
        self.regionstateTxtFld.borderInactiveColor = .buttonsecondary
        self.regionstateTxtFld.placeholderColor = .textcolor
        self.regionstateTxtFld.textColor = .textcolor
        self.regionstateTxtFld.delegate = self
        
       
        downarrow1.image = downarrow1.image?.withRenderingMode(.alwaysTemplate)
        downarrow1.tintColor = UIColor.buttonsecondary

        downarrow2.image = downarrow2.image?.withRenderingMode(.alwaysTemplate)
        downarrow2.tintColor = UIColor.buttonsecondary

        downarrow3.image = downarrow3.image?.withRenderingMode(.alwaysTemplate)
        downarrow3.tintColor = UIColor.buttonsecondary

        downarrow4.image = downarrow4.image?.withRenderingMode(.alwaysTemplate)
        downarrow4.tintColor = UIColor.buttonsecondary

        
        addressTxtView.text = "Residential Address"
        residentialAddrLbl.isHidden = true
        addressTxtView.font = UIFont(name: "Montserrat-Regular", size: 12)
        addressTxtView.delegate = self

        bottomview.backgroundColor = UIColor.buttonsecondary
        bottomviewheight.constant = 1
    }
    @IBAction func backact(_ sender: Any) {
        self.popLeftWithoutAnimation()
    }
    
    @IBAction func postalcodeact(_ sender: Any) {
    }
    @IBAction func nationalityact(_ sender: Any) {
    }
    @IBAction func countryact(_ sender: Any) {
    }
    
    @IBAction func regionstateact(_ sender: Any) {
    }
    
    @IBAction func previousact(_ sender: Any) {
        self.popLeftWithoutAnimation()
        
    }
    @IBAction func nextact(_ sender: Any) {
        
        self.push(id: "KYCViewController3", animation: false, fromSB: Profile)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension KYCViewController2 : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == citytownTxtfFld
        {
        citytownTxtfFld.placeholder = "City/Town"
        }
        
        else if textField == postalcodeTxtFld
        {
        postalcodeTxtFld.placeholder = "Postal Code"
        }
        else if textField == nationalityTxtFld
        {
        nationalityTxtFld.placeholder = "Nationality"
        }
        else if textField == countryTxtFld
        {
        countryTxtFld.placeholder = "Country"
        }
        else if textField == regionstateTxtFld
        {
        regionstateTxtFld.placeholder = "Region/State"
        }
        


    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField.text?.count == 0 {
            
            
            
            if textField == citytownTxtfFld
            {
            citytownTxtfFld.placeholder = "City/Town"
                self.citytownTxtfFld.borderInactiveColor = .buttonsecondary

            }
            
            else if textField == postalcodeTxtFld
            {
            postalcodeTxtFld.placeholder = "Postal Code"
                self.postalcodeTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == nationalityTxtFld
            {
            nationalityTxtFld.placeholder = "Nationality"
                self.nationalityTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == countryTxtFld
            {
            countryTxtFld.placeholder = "Country"
                self.countryTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == regionstateTxtFld
            {
            regionstateTxtFld.placeholder = "Region/State"
                self.regionstateTxtFld.borderInactiveColor = .buttonsecondary

            }
            
            
          
            
        }
    }
    
}

extension KYCViewController2 : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Residential Address" {
            textView.text = nil
            UIView.transition(with: self.view, duration: 0.325, options: .curveEaseInOut, animations: {

                self.bottomview.backgroundColor = UIColor.white
                self.residentialAddrLbl.isHidden = false
                self.bottomviewheight.constant = 2

            }) { finished in

                // completion

            }
            textView.font = UIFont(name: "Montserrat-Regular", size: 17)

        }
    }
   
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Residential Address"
            UIView.transition(with: self.view, duration: 0.325, options: .curveLinear, animations: {

                self.bottomview.backgroundColor = UIColor.buttonsecondary
                self.residentialAddrLbl.isHidden = true
                self.bottomviewheight.constant = 1

            }) { finished in

                // completion

            }

            textView.font = UIFont(name: "Montserrat-Regular", size: 12)


        }
    }

}
