//
//  KYCViewController3.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 04/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
class KYCViewController3: UIViewController {

    @IBOutlet weak var identifiNumberTxtFld: HoshiTextField!
    @IBOutlet weak var identifiTypeTxtFld: HoshiTextField!
    
    @IBOutlet weak var prooffront: UIImageView!
    
    @IBOutlet weak var proofbackbtn: UIButton!
    @IBOutlet weak var prooffrontbtn: UIButton!
    @IBOutlet weak var proofback: UIImageView!
    @IBOutlet weak var frontview: UIView!
    @IBOutlet weak var backview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        frontview.addDashedBorder()
        backview.addDashedBorder()
        
        setDesigns()
        // Do any additional setup after loading the view.
    }
    
 
    func setDesigns(){
        
        self.identifiNumberTxtFld.borderActiveColor = .textcolor
        self.identifiNumberTxtFld.borderInactiveColor = .buttonsecondary
        self.identifiNumberTxtFld.placeholderColor = .textcolor
        self.identifiNumberTxtFld.textColor = .textcolor
        self.identifiNumberTxtFld.delegate = self
        
        self.identifiTypeTxtFld.borderActiveColor = .textcolor
        self.identifiTypeTxtFld.borderInactiveColor = .buttonsecondary
        self.identifiTypeTxtFld.placeholderColor = .textcolor
        self.identifiTypeTxtFld.textColor = .textcolor
        self.identifiTypeTxtFld.delegate = self
       

    }
    
    
    @IBAction func backact(_ sender: Any) {
        self.popLeftWithoutAnimation()
    }
    
    @IBAction func previousact(_ sender: Any) {
        self.popLeftWithoutAnimation()

    }
    @IBAction func nextact(_ sender: Any) {
        
                Common.checkRemoveCdAlert()
                let alert = CDAlertView(title: AppName, message: "KYC Submitted Successfully", type: .success)
        alert.circleFillColor = UIColor.buttonsecondary
        let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonsecondary, handler: nil)
                alert.add(action: doneAction)
                alert.show() { (alert) in
        
                    self.push(id: "EditProfileViewController", animation: true, fromSB: Profile)
                }
    }
    
    @IBAction func prooffront(_ sender: Any) {
    }
    
    @IBAction func proofback(_ sender: Any) {
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
 extension KYCViewController3 : UITextFieldDelegate {
     
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         
         return textField.resignFirstResponder()
     }
     
     func textFieldDidBeginEditing(_ textField: UITextField) {
         
         if textField == identifiNumberTxtFld
         {
         identifiNumberTxtFld.placeholder = "Identification Number"
         }
         
         else if textField == identifiTypeTxtFld
         {
         identifiTypeTxtFld.placeholder = "Identification Type"
         }
                  


     }
     
     func textFieldDidEndEditing(_ textField: UITextField) {
        
         if textField.text?.count == 0 {
             
             
             
             if textField == identifiNumberTxtFld
             {
             identifiNumberTxtFld.placeholder = "Identification Number"
                 self.identifiNumberTxtFld.borderInactiveColor = .buttonsecondary

             }
             
             else if textField == identifiTypeTxtFld
             {
             identifiTypeTxtFld.placeholder = "Identification Type"
                 self.identifiTypeTxtFld.borderInactiveColor = .buttonsecondary

             }
                          
         }
     }
     
 }
