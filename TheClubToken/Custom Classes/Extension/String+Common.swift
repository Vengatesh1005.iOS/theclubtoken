//
//  String+Common.swift
//  Trabezah
//
//  Created by UPLOGIC on 03/10/18.
//  Copyright © 2018 UPLOGIC. All rights reserved.
//

import Foundation
import UIKit
extension String {
    func localized() ->String {
        var appLanguage = ""
        if let languageCode = UserDefaults.standard.value(forKey: "APP_LANGUAGE_CODE") as? String {
            appLanguage = languageCode
        }else {
            appLanguage = "en"
        }
        let path = Bundle.main.path(forResource: appLanguage, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil
            // return true
        } catch {
            return false
        }
    }
    
    var isAlphabetsOnly: Bool {
        return !isEmpty && range(of: "[^a-zA-Z\(" ")]", options: .regularExpression) == nil
    }
    
    func trimmingString() -> String  {
        let trimmedString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmedString
    }
    
    func isContainsWhiteSpace() -> Bool {
        return self.contains(" ")
        //return true
    }
    
    func invalidField(name: String) -> String {
        return "Please enter valid \(name)"
        // return "Invalid credential - Invalid \(name). Kindly try  again"
    }
    
    func invalidLength(fieldName: String, min: Int, max: Int) -> String {
        return "Kindly enter \(fieldName) with a minimum of \(min) and maximum of \(max) characters."
        // return "Kindly enter \(fieldName) with a minimum of \(min) characters and not exceeding \(max) characters"
    }
    
    func getTimeDiffrence()-> String {
        let strTime = self
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date =  formatter.date(from: strTime)
        let calendar = NSCalendar.current
        if date != nil {
            let components =  calendar.dateComponents([.year,.day ,.month,.hour,.minute], from:  date!, to:  Date(timeInterval: -((5*60*60)+(30*60)), since: Date()))
            if components.year != 0 {
                guard components.year == 1 else {
                    return "\(components.year!) years ago"
                }
                return "\(components.year!) year ago"
            } else if components.month != 0 {
                guard components.month == 1 else {
                    return "\(components.month!) months ago"
                }
                return "\(components.month!) month ago"
            } else if components.day != 0 {
                guard components.day == 1 else {
                    return "\(components.day!) days ago"
                }
                return "\(components.day!) day ago"
            } else if components.hour != 0 {
                guard components.hour == 1 else {
                    return "\(components.hour!) hours ago"
                }
                return "\(components.hour!) hour ago"
            } else if components.minute != 0 {
                guard components.minute == 1 else {
                    return "\(components.minute!) mins ago"
                }
                return "\(components.minute!) min ago"
            } else {
                return "just now"
            }
        } else {
            return "Invalid date"
        }
    }
    
    var length: Int { return self.count    }
    
}

extension NSMutableAttributedString {

    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)

        // Swift 4.2 and above
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)

    }

}
