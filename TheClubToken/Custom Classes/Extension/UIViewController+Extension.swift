//
//  UIViewController+Extension.swift
//  SRTN
//
//  Created by cnu on 03/10/17.
//  Copyright © 2017 cnu. All rights reserved.
//

import UIKit

import Reachability
typealias alertBlock = (_ response: Bool) -> Void


extension NSObject{
    
    func showAlertView() {
        let alertMessage = UIAlertController(title: "Mobile Data is Turned Off", message: "Turn on mobile data or use Wi-Fi to access data.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: nil)
        //alertMessage.addAction(settingsAction)
        alertMessage.addAction(okAction)
        
        topMostViewController().present(alertMessage, animated: true, completion: nil)
    }
    
    func showAlertView(message: String){
        let alert = UIAlertController(title: "TCT", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        topMostViewController().present(alert, animated: true, completion: nil)
    }
    
    
    func showAlertWithYesNo(title: String,message: String,callback: @escaping alertBlock)
    {
        let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
              print("Handle Ok logic here")
          
            callback(true)

        }))

        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
              print("Handle Cancel Logic here")
        }))

        topMostViewController().present(refreshAlert, animated: true, completion: nil)

    }
    func showAlertView(title: String,message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        topMostViewController().present(alert, animated: true, completion: nil)
    }
    
    func showAlertView(title: String,message: String,callback: @escaping alertBlock){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            callback(true)
        }
        alert.addAction(okAction)
        topMostViewController().present(alert, animated: true, completion: nil)
    }
    //MARK: - Check response as Dictionary
    
    func isDictionary(response: AnyObject) -> Bool {
        if let result = response as? [String: AnyObject] {
            
            return self.isStatusOk(response: result)
        }
        else {
            return false
        }
    }
    
    func hasInternet() -> Bool {
        let reachability = Reachability.init()
        let networkStatus: Int = reachability!.connection.hashValue
        return (networkStatus != 0)
    }
    
    
    
    
    //MARK: - Check response Status
    func isStatusOk(response: [String: AnyObject]) -> Bool {
        return response["status"] as? Bool ?? false
    }
}
//
//    func showAlert(title: String? = nil, message : String, buttonTitle: String)
//    {
//        if message == "internetError"{
//            self.showInternetError()
//        }else{
//            let alert = FCAlertView()
//
//            let yourImage: UIImage = UIImage(named: "alerticon")!
//
//            alert.colorScheme = UIColor.init(hexString: "#785b2f")
//            alert.cornerRadius = 20
//
//            alert.showAlert(inView: self,
//                            withTitle: title ?? "",
//                            withSubtitle: message,
//                            withCustomImage: yourImage,
//                            withDoneButtonTitle: buttonTitle,
//                            andButtons: nil)
//
//         }
//    }
//
//
//    func showSuccessAlert(title: String? = nil, message : String, buttonTitle: String? = nil)
//    {
//        if message == "internetError"{
//            self.showInternetError()
//        }else{
//
//            let alert = FCAlertView()
//            alert.delegate = self
//            alert.makeAlertTypeSuccess()
//            alert.cornerRadius = 20
//
//            alert.showAlert(inView: self,
//                            withTitle: title ?? "",
//                            withSubtitle: message,
//                            withCustomImage: nil,
//                            withDoneButtonTitle: "OK".localiz(),
//                            andButtons: nil)
//
//        }
//    }
//
//    func showFailureAlert(title: String? = nil, message : String, buttonTitle: String? = nil)
//    {
//        if message == "internetError"{
//            self.showInternetError()
//        }else{
//
//            let alert = FCAlertView()
//            alert.makeAlertTypeWarning()
//            alert.cornerRadius = 20
//
//            alert.showAlert(inView: self,
//                            withTitle: title ?? "",
//                            withSubtitle: message.localiz(),
//                            withCustomImage: nil,
//                            withDoneButtonTitle: "OK".localiz(),
//                            andButtons: nil)
//
//        }
//    }
//    func showAlertCompletion(title: String? = nil, message : String, buttonTitle: String? = nil,completionBlock1:@escaping () -> ())
//    {
//        if message == "internetError"{
//            self.showInternetError()
//        }else{
//            let alertController = UIAlertController(title: title ?? "", message: message, preferredStyle: .alert)
//            let OKAction = UIAlertAction(title:  "OK".localiz(), style: .default) {
//                (action: UIAlertAction) in
//                completionBlock1()
//            }
//            alertController.addAction(OKAction)
//            self.present(alertController, animated: true, completion: nil)
//        }
//
//    }
//
//    func showAlert2Buttons(title: String? = nil, message : String, buttonTitle1: String, buttonTitle2: String, completionBlock1:@escaping () -> (), completionBlock2:@escaping () -> ())
//    {
//        let alertController = UIAlertController(title: title ?? "", message: message, preferredStyle: .alert)
//        let action1 = UIAlertAction(title: buttonTitle1, style: .default) {
//            (action: UIAlertAction) in
//            completionBlock1()
//        }
//        alertController.addAction(action1)
//        let action2 = UIAlertAction(title: buttonTitle2, style: .cancel) {
//            (action: UIAlertAction) in
//            completionBlock2()
//        }
//        alertController.addAction(action2)
//        self.present(alertController, animated: true, completion: nil)
//    }
//
//    func showAlert(title: String? = nil, message : String, buttonTitle: String? = nil, completionHandler:@escaping () -> ())
//    {
//        let alertController = UIAlertController(title: title ?? "", message: message, preferredStyle: .alert)
//        let OKAction = UIAlertAction(title: buttonTitle ?? "OK", style: .default) {
//            (action: UIAlertAction) in
//            completionHandler()
//        }
//        alertController.addAction(OKAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
//
//    func showMenuUnFollowButtons(title: String? = nil, message : String, buttonTitle1: String, buttonTitle2: String,  buttonTitle3: String,  completionBlock1:@escaping () -> (), completionBlock2:@escaping () -> (), completionBlock3:@escaping () -> ())
//    {
//        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
//        var actionarray = [UIAlertAction]()
//
//        let follow = UIAlertAction(title: buttonTitle1, style: .default) {
//            (action: UIAlertAction) in
//            completionBlock1()
//        }
//        follow.setValue(UIColor.white, forKey: "titleTextColor")
//        actionarray.append(follow)
//
//        let PostNotification = UIAlertAction(title: buttonTitle2, style: .default) {
//            (action: UIAlertAction) in
//            completionBlock2()
//        }
//        PostNotification.setValue(UIColor.white, forKey: "titleTextColor")
//        actionarray.append(PostNotification)
//
//        let cancel = UIAlertAction(title: buttonTitle3, style: .cancel) {
//            (action: UIAlertAction) in
//            completionBlock3()
//        }
//        cancel.setValue(UIColor.white, forKey: "titleTextColor")
//        actionarray.append(cancel)
//
//        let alertContentArray = [buttonTitle1, buttonTitle2, buttonTitle3]
//        var itemsArray = [NSMutableAttributedString]()
//        var myMutableString = NSMutableAttributedString()
//
//        for name in alertContentArray {
//            if name == "Cancel"  {
//                myMutableString = NSMutableAttributedString(string: name as String, attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 20.0, weight: .semibold)])
//            } else {
//                myMutableString = NSMutableAttributedString(string: name as String, attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 20.0, weight: .regular)])
//            }
//            itemsArray.append(myMutableString)
//        }
//
//        optionMenu.addAction(PostNotification)
//        optionMenu.addAction(follow)
//        optionMenu.addAction(cancel)
//
//        self.present(optionMenu, animated: true, completion: nil)
//
//    }
    
    


extension NSObject{
    public func topMostViewController() -> UIViewController {
        return self.topMostViewController(withRootViewController: (UIApplication.shared.keyWindow?.rootViewController!)!)
    }
    public func topMostViewController(withRootViewController rootViewController: UIViewController) -> UIViewController {
//        if (rootViewController is UITabBarController) {
//            let tabBarController = (rootViewController as! UITabBarController)
//            return self.topMostViewController(withRootViewController: tabBarController.selectedViewController!)
//        }
         if (rootViewController is UINavigationController) {
            let navigationController = (rootViewController as! UINavigationController)
            return self.topMostViewController(withRootViewController: navigationController.visibleViewController!)
        }
        else if rootViewController.presentedViewController != nil {
            let presentedViewController = rootViewController.presentedViewController!
            return self.topMostViewController(withRootViewController: presentedViewController)
        }
        else {
            return rootViewController
        }
        
    }
}

@objc protocol ActivityIndicatorDelegate {
    func startActivityIndicator()
    func stopActivityIndicator()
}

extension UIViewController:ActivityIndicatorDelegate{

    var activityIndicatorHoldingViewTag: Int { return 999999 }
    
    func startActivityIndicator(){
        DispatchQueue.main.async{
            //create holding view
            let holdingView = UIView(frame: UIScreen.main.bounds)
            holdingView.backgroundColor = .black
            holdingView.alpha = 0.3
            
            //Add the tag so we can find the view in order to remove it later
            holdingView.tag = self.activityIndicatorHoldingViewTag
            
            //create activity indicator
            let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            
            //Start animating and add the view
            activityIndicator.startAnimating()
            holdingView.addSubview(activityIndicator)
            self.view.addSubview(holdingView)
        }
    }
    
    func stopActivityIndicator() {
        DispatchQueue.main.async{
            //Here we find the `UIActivityIndicatorView` and remove it from the view
            if let holdingView = self.view.subviews.filter({ $0.tag == self.activityIndicatorHoldingViewTag}).first {
                holdingView.removeFromSuperview()
            }
        }
    }
    
    func setStatusBarBackgroundColor(color: UIColor) {
        
        guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey:"statusBar") as? UIView else {
            return
        }
        
        statusBar.backgroundColor = color
    }
    
    func presentModallySimilarToPush(_ vc:UIViewController, animated: Bool, completion:(()->())? = nil){
        let transition = CATransition()
//        transition.duration = 0.5
//        transition.type = kCATransitionPush
//        transition.subtype = kCATransitionFromRight
//        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(vc, animated: animated, completion: completion)
    }
    
    func dimissSimilarToPop(animated: Bool, completion:(()->())? = nil){
        let transition = CATransition()
//        transition.duration = 0.5
//        transition.type = kCATransitionPush
//        transition.subtype = kCATransitionFromLeft
//        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.dismiss(animated: animated, completion: completion)
    }
    
    func showBluetoothError(){
        
    }
    
    
    private func getBannerView(text:String)->UIView{
        let bannerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100))
        bannerView.backgroundColor = UIColor.white
        let label = UILabel(frame: CGRect(x: 20, y: 20, width: self.view.frame.size.width - 40, height: 60))
        label.text = text
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 16)
        label.textAlignment = .center
        label.contentMode = .center
        label.numberOfLines = 0
        label.sizeToFit()
        bannerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: label.frame.size.height + 40 )
        bannerView.addSubview(label)
        if label.frame.size.width < self.view.frame.size.width - 40{
            label.frame = CGRect(x: label.frame.origin.x, y: label.frame.origin.y, width: self.view.frame.size.width - 40, height: label.frame.size.height)
        }
        bannerView.clipsToBounds = true
        return bannerView
    }
    
    private func getBannerView(title:String, subTitle:String, titleColor:UIColor, subTitleColor:UIColor, titleFont:UIFont, subTitleFont:UIFont )->UIView{
        let bannerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100))
        bannerView.backgroundColor = UIColor.white
        let titleLabel = UILabel(frame: CGRect(x: 20, y: 20, width: self.view.frame.size.width - 40, height: 60))
        titleLabel.text = title
        titleLabel.textColor = .white
        titleLabel.font = titleFont
        titleLabel.contentMode = .center
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        titleLabel.sizeToFit()
        bannerView.addSubview(titleLabel)
        if titleLabel.frame.size.width < self.view.frame.size.width - 40{
            titleLabel.frame = CGRect(x: titleLabel.frame.origin.x, y: titleLabel.frame.origin.y, width: self.view.frame.size.width - 40, height: titleLabel.frame.size.height)
        }
        
        let subtitleLabel = UILabel(frame: CGRect(x: 20, y: titleLabel.frame.maxY + 8, width: self.view.frame.size.width - 40, height: 60))
        subtitleLabel.text = subTitle
        subtitleLabel.textColor = .white
        subtitleLabel.font = subTitleFont
        subtitleLabel.contentMode = .center
        subtitleLabel.textAlignment = .center
        subtitleLabel.numberOfLines = 0
        subtitleLabel.sizeToFit()
        bannerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: subtitleLabel.frame.maxY + 20 )
        bannerView.addSubview(subtitleLabel)
        
        if subtitleLabel.frame.size.width < self.view.frame.size.width - 40{
            subtitleLabel.frame = CGRect(x: subtitleLabel.frame.origin.x, y: subtitleLabel.frame.origin.y, width: self.view.frame.size.width - 40, height: subtitleLabel.frame.size.height)
        }
        
        bannerView.clipsToBounds = true
        return bannerView
    }
    
//    func showBanner(text:String){
//        let bannerView = self.getBannerView(text: text)
//        self.view.addSubview(bannerView)
//        let height = bannerView.frame.height
//        bannerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 0 )
//
//
////        let modalWindow:UIWindow = UIWindow(frame: self.view.window!.bounds)
////
////        modalWindow.backgroundColor = UIColor.clear
////        modalWindow.isHidden = false
////        modalWindow.windowLevel = (UIWindowLevelStatusBar + 1)
////
////        modalWindow.addSubview(bannerView)
////
////        modalWindow.makeKeyAndVisible()
//        guard let applicationDelegate = UIApplication.shared.delegate as? AppDelegate, let window:UIWindow = applicationDelegate.window else{
//            return
//        }
//        let level = window.windowLevel
//        window.windowLevel = UIWindow.Level.statusBar + 1
//        window.makeKeyAndVisible()
//        window.addSubview(bannerView)
//        bannerView.layer.zPosition = 1
//        UIView.animate(withDuration: 0.5, animations: {
//            bannerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: height)
//        }, completion: {val in
//            UIView.animate(withDuration: 0.5, delay: 1.5, options: [], animations: {
//                bannerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 0)
//            }, completion: {val in
//                for v in bannerView.subviews{
//                    v.isHidden = true
//                }
//                bannerView.removeFromSuperview()
//                window.windowLevel = level
//                window.makeKeyAndVisible()
////                guard let applicationDelegate = UIApplication.shared.delegate as? AppDelegate, let window:UIWindow = applicationDelegate.window else{
////                    return
////                }
//            })
//        })
//
//    }
//
//    func showInternetError(){
//        let bannerView = self.getBannerView(title: "Hey, It looks like you're offline.", subTitle: "Please turn on Mobile Data or WiFi", titleColor: .white, subTitleColor: .white, titleFont: UIFont.systemFont(ofSize: 16), subTitleFont: UIFont.systemFont(ofSize: 14))
//        self.view.addSubview(bannerView)
//        bannerView.backgroundColor = UIColor.black
//        let height = bannerView.frame.height
//        bannerView.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: self.view.frame.size.width, height: 0 )
//
//
//        guard let applicationDelegate = UIApplication.shared.delegate as? AppDelegate, let window:UIWindow = applicationDelegate.window else{
//            return
//        }
//        let level = window.windowLevel
//        window.windowLevel = UIWindow.Level.statusBar + 1
//        window.makeKeyAndVisible()
//        window.addSubview(bannerView)
//        bannerView.layer.zPosition = 1
//        UIView.animate(withDuration: 0.5, animations: {
//            bannerView.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - height, width: self.view.frame.size.width, height: height)
//        }, completion: {val in
//            UIView.animate(withDuration: 0.5, delay: 1.5, options: [], animations: {
//                bannerView.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: self.view.frame.size.width, height: height)
//            }, completion: {val in
//                for v in bannerView.subviews{
//                    v.isHidden = true
//                }
//                bannerView.removeFromSuperview()
//                window.windowLevel = level
//                window.makeKeyAndVisible()
//            })
//        })
//
//    }
}
//extension UIViewController:FCAlertViewDelegate
//{
//    public func fcAlertDoneButtonClicked(_ alertView: FCAlertView!) {
//
//
//        }
//
//}
