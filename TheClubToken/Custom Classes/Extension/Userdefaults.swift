//
//  Userdefaults.swift
//  Noor
//
//  Created by Uplogic-user on 21/06/17.
//  Copyright © 2017 Uplogic. All rights reserved.
//

import UIKit

extension UserDefaults{
    struct userdefaults {
        static let login = "logininfo"
        static let header = "headers"
    }
    
    
    func saveLoginDetails(logininfo : [String : Any]){
        if logininfo.count > 0 {
            self.set(logininfo, forKey: userdefaults.login)
            self.synchronize()
        }
    }
    
    func clearLoginDetails(){
        self.removeObject(forKey: userdefaults.login)
        self.removeObject(forKey: userdefaults.header)
        self.synchronize()
    }
    
    func getLoggedUserDetails() -> [String : Any] {
        if let _ = self.object(forKey: userdefaults.login) {
            return self.object(forKey: userdefaults.login) as! [String : AnyObject]
        }
        return [:]
    }

}
