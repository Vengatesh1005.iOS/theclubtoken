//
//  MediaPicker.swift
//  HighburDriver
//
//  Created by Uplogic-user on 13/01/17.
//  Copyright © 2017 Uplogic. All rights reserved.
//

import UIKit

typealias MediaCompletionBlock = (_ image: UIImage?,_ status: Bool) -> Void

class MediaPicker: NSObject,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    static let shared = MediaPicker()
    var mediaImageView: UIImageView!
    var mediaCompletionBlock: MediaCompletionBlock?
    var isEditing = true
    
    func showMediaPicker(imageView: UIImageView,placeHolder: UIImage?,isediting: Bool=true, sender: UIButton){
        self.mediaImageView = imageView
        isEditing = isediting
        let alertMessage = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Take Photo", style: .default) { (action) in
            self.showCamera()
        }
        let photos = UIAlertAction(title: "Choose Photo", style: .default) { (action) in
            self.showPhotoLibrary()
        }
        /*let remove = UIAlertAction(title: "Remove Photo", style: .default) { (action) in
         
         imageView.image = placeHolder
         if(self.mediaCompletionBlock != nil){
         self.mediaCompletionBlock!(nil,false)
         }
         }*/
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertMessage.addAction(camera)
        alertMessage.addAction(photos)
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alertMessage.popoverPresentationController?.sourceView = sender
            alertMessage.popoverPresentationController?.sourceRect = sender.bounds
            alertMessage.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        alertMessage.addAction(cancel)
        topMostViewController().present(alertMessage, animated: true, completion: nil)
    }
    
    func showMediaPicker(imageView: UIImageView,placeHolder: UIImage?, sender: UIButton, editing: Bool = false,completion: @escaping MediaCompletionBlock){
        isEditing = editing
        mediaCompletionBlock = completion
        self.showMediaPicker(imageView: imageView, placeHolder: placeHolder, sender: sender)
    }
    
    func showCamera(completion: @escaping MediaCompletionBlock){
        mediaImageView = nil
        mediaCompletionBlock = completion
        showCamera()
    }
    
    func showPhotoLibrary(completion: @escaping MediaCompletionBlock){
        mediaImageView = nil
        mediaCompletionBlock = completion
        showPhotoLibrary()
    }
    
    func showCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            
            let imag = UIImagePickerController()
            self.configPicker(imag: imag)
            imag.sourceType = UIImagePickerController.SourceType.camera;
            topMostViewController().present(imag, animated: true, completion: nil)
        }
    }
    
    func showPhotoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imag = UIImagePickerController()
            self.configPicker(imag: imag)
            imag.sourceType = UIImagePickerController.SourceType.photoLibrary;
            topMostViewController().present(imag, animated: true, completion: nil)
        }
    }
    
    func configPicker(imag : UIImagePickerController){
        imag.navigationBar.isTranslucent = false
        imag.navigationBar.barTintColor = UIColor.black // Background color
        imag.navigationBar.tintColor = UIColor.white// Cancel button ~ any UITabBarButton items
        imag.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
        imag.delegate = self
        imag.allowsEditing = isEditing
    }
    
    //MARK:- UIImagePickerControllerDelegate
    @objc func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
     let image_crop : UIImage = editingInfo[UIImagePickerController.InfoKey.originalImage] as! UIImage
     let resizeimage = self.resizeImage(image: image, newWidth: self.mediaImageView.frame.width)
     self.mediaImageView.image = image_crop
        print(self.mediaImageView.image)
     if(mediaCompletionBlock != nil){
     mediaCompletionBlock!(resizeimage,true)
     mediaCompletionBlock = nil
     }
     topMostViewController().dismiss(animated: true, completion: nil)
     }
    
    
    
//   @objc func imagePickerController(_ picker: UIImagePickerController,
//                               didFinishPickingMediaWithInfo info: [String : Any]){
//        if(isEditing){
//            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
//            {
//
//               let resizeimage = self.resizeImage(image: image, newWidth: 400)
//                if(self.mediaImageView != nil){
//                    self.mediaImageView.image = resizeimage
//                }
//
//                if(mediaCompletionBlock != nil){
//                    mediaCompletionBlock!(image,true)
//                    mediaCompletionBlock = nil
//                    isEditing = false
//                }
//            }
//        }
//        else{
//            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
//            {
//                //let resizeimage = self.resizeImage(image: image, newWidth: self.mediaImageView.width)
//                if(self.mediaImageView != nil){
//                    self.mediaImageView.image = image
//                }
//
//                if(mediaCompletionBlock != nil){
//                    mediaCompletionBlock!(image,true)
//                    mediaCompletionBlock = nil
//                }
//            }
//        }
//        topMostViewController().dismiss(animated: true, completion: nil)
//    }
//
   
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        if(mediaCompletionBlock != nil){
            mediaCompletionBlock!(nil,false)
            mediaCompletionBlock = nil
        }
        topMostViewController().dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        //UIGraphicsBeginImageContext(CGSize(width: newWidth,height: newHeight))
        UIGraphicsBeginImageContextWithOptions(CGSize(width: newWidth*2,height: newHeight*2), false, image.scale)
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth*2, height: newHeight*2))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

