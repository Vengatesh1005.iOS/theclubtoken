//
//  Colors.swift
//  User
//
//  Created by imac on 12/22/17.
//

import UIKit

enum Color : Int {
    
    case primary = 1
    case secondary = 2
    case lightBlue = 3
    case brightBlue = 4
    
    
    static func valueFor(id : Int)->UIColor?{
        
        switch id {
            
            
        case self.primary.rawValue:
            return .primarybackground
            
        case self.secondary.rawValue:
            return .secondarybackground
            
            
        case self.brightBlue.rawValue:
            return .brightBlue
            
        default:
            return nil
        }
    }
}

extension UIColor {
    
    static var primary : UIColor {
           
           return UIColor(red: 149/255, green: 116/255, blue: 205/255, alpha: 1)
       }
      
    // Primary Color
    static var primarybackground : UIColor {
        

        return UIColor(red: 37/255, green: 7/255, blue: 59/255, alpha: 1)
    }
    
    // Secondary Color
    static var secondarybackground : UIColor {

        return UIColor(red: 60/255, green: 31/255, blue: 79/255, alpha: 1)

    }
    
   static var textcolor : UIColor {

       return UIColor(red: 247/255, green: 237/255, blue: 248/255, alpha: 1)

   }
    
    static var buttonprimary : UIColor {

        return UIColor(red: 184/255, green: 53/255, blue: 255/255, alpha: 1)

    }
    static var buttonsecondary : UIColor {

        return UIColor(red: 110/255, green: 28/255, blue: 162/255, alpha: 1)

    }
    static var MenuFirstIndex : UIColor {
        return UIColor(red: 114/255, green: 209/255, blue: 204/255, alpha: 1)
    }
    static var MenuSecondIndex : UIColor {
        return UIColor(red: 154/255, green: 222/255, blue: 219/255, alpha: 1)
    }
    
    //Gradient Start Color
    
    static var startGradient : UIColor {
        return UIColor(red: 83/255, green: 173/255, blue: 46/255, alpha: 1)
    }
    
    //Gradient End Color
    
    static var endGradient : UIColor {
        return UIColor(red: 158/255, green: 178/255, blue: 45/255, alpha: 1)
    }
    
    // Blue Color
    
    static var brightBlue : UIColor {
        return UIColor(red: 40/255, green: 25/255, blue: 255/255, alpha: 1)
    }
    
    static func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
    static let backgroundColor = UIColor.rgb(r: 21, g: 22, b: 33)
    static let outlineStrokeColor = UIColor.rgb(r: 234, g: 46, b: 111)
    static let trackStrokeColor = UIColor.rgb(r: 56, g: 25, b: 49)
    static let pulsatingFillColor = UIColor.rgb(r: 86, g: 30, b: 63)
    
    
}
