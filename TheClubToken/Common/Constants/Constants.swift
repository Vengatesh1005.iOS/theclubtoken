//
//  Constants.swift
//  Centros_Camprios
//
//  Created by imac on 12/18/17.
//  Copyright © 2017 Appoets. All rights reserved.
//

import UIKit
import Foundation

import SwiftyJSON



// MARK: - User Detail

var userDetail = JSON()
var appSettting = JSON()
var firDict = NSDictionary()

// MARK: - Constant Strings
 var appName = "TheClubToken"
 var appNameShort = "TCT"
 var pinfrom = ""
 var enteredpin = ""
 var touchIDfrom = ""
 var faceIDfrom = ""

 var otpMail = ""
 let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate

let Login = UIStoryboard(name: "Login", bundle: Bundle.main)
let Profile = UIStoryboard(name: "Profile", bundle: Bundle.main)
let Tabbar = UIStoryboard(name: "TabbarStoryboard", bundle: Bundle.main)


var firebaseDictArray = NSMutableArray()



// MARK: - Currency Detail
var allCurrency = [currencyval]()
var currency = ""
var symbol = ""
var rapid_api_url = ""
var rapid_api_key = ""
var sms_status = ""
var lat_long = [String]()
struct currencyval {
    var shortname = String()
    var symbol = String()
}

enum Environment: String {
    case Development = "dev"
    case Production = "production"
    case LocalHost = "localhost"
}
struct Constants {
    static let string = Constants()
    
    var window: UIWindow?
    var tripStatus: pushNotification!
    let notificationName = Notification.Name("tracking")
    var device_token = "123"
    let USERDEFAULTS = UserDefaults.standard
    let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
    let STORYBOARDS = UIStoryboard(name: "Main", bundle: nil)
  
    let APP_STORE_URL = ""
    let APP_STORE_APP_URL = ""
    let API_VERSION = "1"
   
    let environment = Environment.Development
    var PARTNERID = ""
    var CUSTOMERID = ""
    var userDetails = JSON()
    
    var emailVerify = Int()
    var email2otp = Int()
    var successMsg = ""
    var user_Id = Int()
    var email_check = ""
    var email_default = ""
    var pass_check = ""
    var QrCodeContect = ""
   
    
//    var driverstatusRef: DatabaseReference! = nil
//    var oldselectedcell: CarCategoryCell! = nil
//    var cashstatusRef: DatabaseReference! = nil
//    var geoFire: GeoFire! = nil
//    var geofireRef: DatabaseReference! = nil
//    var regionQuery: GFCircleQuery! = nil
    
    var oldselectedindex: Int! = 0
    var paymentCompletion = false
    var paymentname: String = ""
    var paypalid: String = ""
    var otpstatus = true
    var queryenterhandle: UInt! = nil
    var queryexithandle: UInt! = nil
    var vehicleid = ""
    var selectedcartype = ""
    var selectedcarprice = "1"
    var selectedcarbaseprice = "1"
    var drivers_id = ""
    
    
    let writeSomething = "Write Something"
    let noChatHistory = "No Chat History Found"
    let yes = "Yes"
    let no = "No"
    let newVersion = "This is a newer version is available for download!Please update the app by visiting the App Store"
    let Done = "Done"
    let Back = "Back"
    let delete = "Delete"
    let noDevice = "no device"
    let manual = "manual"
    let OK = "OK"
    let Cancel = "Cancel"
    let NA = "NA"
    let MobileNumber = "Mobile Number"
    let next = "Next"
    let selectSource = "Select Source"
    let ConfirmPassword = "ConfirmPassword"
    let camera = "Camera"
    let photoLibrary = "Photo Library"
    let walkthrough = "Walkthrough"
    let signIn = "LOGIN"
    let signUp = "REGISTER NOW"
    let orConnectWithSocial = "Or connect with social"
    let changePassword = "Change Password"
    let resetPassword = "Reset Password"
    let enterOtp = "Enter OTP"
    let otpIncorrect = "OTP incorrect"
    let enterCurrentPassword = "Current Password"
    let walkthroughWelcome = "Services to transport you where you need to go. A lot can happen, on our ride."
    let walkthroughDrive = "Take a ride whenever and wherever you want. Plan and Schedule for us to pick you up."
    let walkthroughEarn = "We have the most friendly drivers who will go the extra mile for you."
    let welcome = "Welcome"
    let schedule = "Schedule"
    let drivers = "Drivers"
    let driverName = "Driver Name"
    let country = "Country"
    let timeZone = "Time Zone"
    let referalCode = "Referral Code"
    let business = "Business"
    let emailPlaceHolder = "name@example.com"
    let email = "Email"
    let emergencyPhonenumber1 = "Emergency PhoneNumber1"
    let emergencyPhoenumber2 = "Emergency PhoneNumber2"
    let iNeedTocreateAnAccount = "I need to create an account"
    let whatsYourEmailAddress = "What's your mobile number?"
    let welcomeBackPassword = "Welcome back, sign in to continue"
    let enterPassword = "Enter Password"
    let enterNewpassword = "Enter New Password"
    let enterConfirmPassword = "Enter Confirm Password"
    let password = "Password"
    let newPassword = "New Password"
    let iForgotPassword = "I forgot my password"
    let enterYourMailIdForrecovery = "Enter your mail ID for recovery"
    let registerDetails = "Enter the details to register"
    let chooseAnAccount = "Choose an account"
    let facebook = "Facebook"
    let google = "Google"
    let payment = "Payment"
    let yourTrips = "My Trips"
    let coupon = "Coupon"
    let couponnApplied = "Coupon Applied"
    let wallet = "Wallet"
    let passbook = "Passbook"
    let settings = "Settings"
    let help = "Help"
    let aboutUs = "About us"
    let share = "Share"
    let inviteReferral = "Invite Referral"
    let faqSupport = "FAQ Support"
    let termsAndConditions = "Terms and Conditions"
    let privacyPolicy = "Privacy Policy"
    let logout = "Logout"
    let profile = "Profile"
    let first = "First Name"
    let last = "Last Name"
    let phoneNumber = "Phone Number"
    let tripType = "Trip Trip"
    let personal = "Personal"
    let save = "save"
    let lookingToChangePassword = "Looking to change password?"
    let areYouSure = "Are you sure?"
    let areYouSureWantToLogout = "Are you sure want to logout?"
    let sure = "Sure"
    let source = "Source"
    let destination = "Destination"
    let enterDestination = "Enter Destination address"
    let destinationIsEmpty = "Enter destination Address"
    let home = "Home"
    let work = "Work"
    let addLocation = "Add Location"
    let selectService = "Select Service"
    let service = "Service"
    let more = "More"
    let change = "change"
    let getPricing = "GET PRICING"
    let cancelRequest = "Cancel Request"
    let cancelRequestDescription = "Are you sure want to cancel the request?"
    let findingDriver = "Finding Driver..."
    let dueToHighDemandPriceMayVary = "Due to high demand price may vary"
    let estimatedFare = "Estimated Fare"
    let ETA = "ETA"
    let model = "Model"
    let useWalletAmount = "Use Wallet Amount"
    let scheduleRide = "schedule ride"
    let rideNow = "ride now"
    let scheduleARide = "Schedule your Ride"
    let select = "Select"
    let pickaDate = "Pick a date"
    let pickaReturndate = "Pick a return date"
    let driverAccepted = "Driver accepted your request."
    let youAreOnRide = "You are on ride."
    let bookingId = "Booking ID"
    let distanceTravelled = "Total Distance (Km)"
    let timeTaken = "Total Riding Time (Mins)"
    let baseFare = "Base Fare"
    let baseFareKm = "Base Fare (First 2km)"
    let cash = "Cash"
    let Card = "Card"
    let CCAvenue = "CCAvenue"
    let paynow = "Pay Now"
    let rateyourtrip = "Rate your trip with"
    let writeYourComments = "Write your comments"
    let distanceFare = "Distance Fare"
    let tax = "Tax"
    let total = "Total"
    let submit = "Submit"
    let driverArrived = "Driver has arrived at your location."
    let peakInfo = "Due to peak hours, charges will be varied based on availability of provider."
    let call = "Call"
    let past = "Past"
    let upcoming = "Upcoming"
    let addCardPayments = "Add card for payments"
    let paymentMethods = "Payment Methods"
    let yourCards = "Your Cards"
    let walletHistory = "Wallet History"
    let couponHistory = "Coupon History"
    let enterCouponCode = "Enter Coupon Code"
    let addCouponCode = "Add Coupon Code"
    let resetPasswordDescription = "Note : Please enter the OTP send to your registered email address"
    let latitude = "latitude"
    let longitude = "longitude"
    let totalDistance = "Total Distance"
    let shareRide = "Share Ride"
    let wouldLikeToShare = "would like to share a ride with you at"
    let profileUpdated = "Profile updated successfully"
    let otp = "OTP"
    let at = "at"
    let favourites = "Favourites"
    let changeLanguage = "Change Language"
    let noFavouritesFound = "No favourite address available"
    let cannotMakeCallAtThisMoment = "Cannot make call at this moment"
    let offer = "Offer"
    let amount = "Amount"
    let creditedBy = "Credited By"
    let CouponCode = "Coupon Code"
    let OFF = "OFF"
    let couldnotOpenEmailAttheMoment = "Could not open Email at the moment."
    let couldNotReachTheHost = "Could not reach th web"
    let wouldyouLiketoMakeaSOSCall = "Would you like to make a SOS Call?"
    let mins = "mins"
    let invoice = "Invoice"
    let viewRecipt = "View Recipt"
    let payVia = "Pay via"
    let comments = "Comments"
    let pastTripDetails = "Past Trip Details"
    let upcomingTripDetails = "Upcoming Trip Details"
    let paymentMethod = "Payment Method"
    let cancelRide = "Cancel Ride"
    let noComments = "no Comments"
    let noPastTrips = "No Past Trips"
    let noUpcomingTrips = "No Upcoming Trips"
    let noWalletHistory = "No Wallet Details"
    let noCouponDetail = "No Coupon Details"
    let fare = "Fare"
    let fareType = "Fare Type"
    let capacity = "Capacity"
    let rateCard = "Rate Card"
    let distance = "Distance Price"
    let sendMyLocation = "Send my Location"
    let noInternet = "No Internet?"
    let bookNowOffline = "Book Now using SMS"
    let tapForCurrentLocation = "Tap the button below to send your current location by SMS."
    let standardChargesApply = "Standard charges may apply"
    let noThanks = "No thanks, I'll try later"
    let iNeedCab = "I need a cab @"
    let donotEditMessage = "(Please donot edit this SMS. Standard SMS charges of Rs.3 per SMS may apply)"
    let pleaseTryAgain = "Please try again"
    let faq = "FAQ"
    let document = "Document"
    
    let ADDCOUPON = "ADD COUPON"
    let addAmount = "Add Money"
    let ADDAMT = "ADD AMOUNT"
    let yourWalletAmnt = "Your wallet amount is"
    let Support = "Support"
    let helpQuotes = "Our team persons will contact you soon!"
    let areYouSureCard = "Are you sure want to delete this card?"
    let remove = "Remove"
    let userAlreadyExists = "User already exists"
    let discount = "ETA Discount"
    let planChanged = "Plan Changed"
    let bookedAnotherCab = "Booked another cab"
    let driverDelayed = "Driver Delayed"
    let lostWallet = "Lost Wallet"
    let othersIfAny = "Others If Any"
    let reasonForCancellation = "Reason For Cancellation"
    let addCard = "Add Card to continue with wallet"
    let carNumber = "Car plateNumber"
    let enterValidAmount = "Enter Valid Amount"
    let RentserviceIsNOtAvailble = "Rental service is not available now."
    let enterthedetailstoRegister = "Enter the details to register"
    let verifyOTP = "Verify OTP"
    let OTPsent = "OTP sent to your mobile number"
    let resendOtp = "Resent OTP"
    let callfotOTP = "Call for OTP"
    let confirm = "Confirm"
    let rentACar = "Rent a Car"
    let outstation = "Book you Outstation ride"
    let pickUpLocation = "PICKUP LOCATION"
    let selectPackage = "SELECT PACKAGE"
    let selectDateAndTime = "SELECT DATE AND TIME"
    let chooseaCarType = "CHOOSE A CAR TYPE"
    let additionalDetails = "ADDITIONAL DETAILS"
    let fareDetails = "FARE DETAILS"
    let fareDetail1 = "Charges applicable upon exceeding hours/kms"
    let fareDetail2 = "Additional TAX applicable on final bill"
    let fareDetail3 = "Toll, if applicable will be auto-added to bill"
    let fareDetail4 = "For local travel only"
    
    let wheelChair = "Wheal Chair"
    let childSeat = "Child Seat"
    
    
    let oneWay = "One way"
    let roundTrip = "Round trip"
    let getDroppedOff = "Get Dropped Off"
    let keepCarTillReturn = "Keep Car till return"
    
    let selectPackageAndCarType = "Select the package and car Type"
    let leaveOn = "Leave on"
    let returnBy = "Return by"
    let leaveonIsEmpty = "Leave on filed is empty"
    let returnByEmpty = "Return by filed is empty"
    
    let peakHour = "Peak Hour Charges"
    let nightFare = "Night Fare"
    let driverAllowance = "Driver Allowance"
    let totalRidingCharges = "Total riding Charges"
    let gst = "GST"
    let customerPayableAmount = "Payable"
    let commission = "Commission"
    let providerEarnings = "Provider Earnings"
    
}

enum myServiceCategory: String {
    case Gas_selling_service = "gassellingservice"
    case Brake_down = "brakedown"
    case Plumber = "plumber"
}

enum pushNotification: String {
    case tripSendRequest = "Service Request"
    case tripAccept = "Service Accept"
    case tripStart = "Service Start"
    case tripComplete = "Service Complete"
    case payConfirm = "Service Paid"
}

enum tripstatus: String {
    case norequest = "0"
    case tripSendRequest = "1"
    case tripAccept = "2"
    case tripStart = "3"
    case tripComplete = "4"
    case paymentComplete = "6"
    case customerCancel = "8"
    case driverCancel = "9"
    case payConfirm = "Service Paid"
    case adminCancel = "5"
    
   
}


//ENUM TRIP TYPE

enum TripType : String, Codable {
    
    case Business
    case Personal
    
}

//Payment Type

enum  PaymentType : String, Codable {
    
    case CASH = "CASH"
    case ccAvenue = "CC_AVENUE"
    case CARD = "CARD"
}


// Date Formats

struct DateFormat {
    
    static let list = DateFormat()
    let yyyy_mm_dd_HH_MM_ss = "yyyy-MM-dd HH:mm:ss"
    let MMM_dd_yyyy_hh_mm_ss_a = "MMM dd, yyyy hh:mm:ss a"
    let hhmmddMMMyyyy = "hh:mm a - dd:MMM:yyyy"
    let ddMMyyyyhhmma = "dd-MM-yyyy hh:mma"
    let ddMMMyyyy = "dd MMM yyyy"
    let hh_mm_a = "hh : mm a"
        let dd_mm_yyyy = "dd/MM/yyyy"
}


// Devices

enum DeviceType : String, Codable {
    
    case ios = "ios"
    case android = "android"
    
}

//Lanugage

enum Language : String {
    case english = "en"
   // case spanish = "Spanish"
    static var count: Int{ return 1 }
}



// MARK:- Login Type

enum LoginType : String, Codable {
    
    case facebook
    case google
    case manual
    
}

enum myServiceCategoryType: String {
    
    case Gas_selling_service = "gassellingservice"
    case Brake_down = "brakedown"
    case Plumber = "plumber"
}

enum pushNotificationType: String {
    
    case tripSendRequest = "Service Request"
    case tripAccept = "Service Accept"
    case tripStart = "Service Start"
    case tripComplete = "Service Complete"
    case payConfirm = "Service Paid"
}

enum tripStatus: String {
    
    case tripSendRequest = "1"
    case tripAccept = "2"
    case tripStart = "3"
    case tripComplete = "4"
    case customerCancel = "8"
    case driverCancel = "9"
    case payConfirm = "Service Paid"
}


enum Vibration : UInt {
    case weak = 1519
    case threeBooms = 1107
}

 func toString(_ anything: Any?) -> String {
    if let any = anything {
        if let num = any as? NSNumber {
            return num.stringValue
        } else if let str = any as? String {
            return str
        }
    }
    return ""
    
}
