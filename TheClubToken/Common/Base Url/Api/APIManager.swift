//
//  APIManager.swift
//  Noor
//
//  Created by Uplogic-user on 19/05/17.
//  Copyright © 2017 Uplogic. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation
import SVProgressHUD
import SwiftyJSON

typealias coordinatesCompletionBlock = (_ coordinate: CLLocationCoordinate2D?) -> Void
typealias responseCompletionBlock = (_ response: JSON) -> Void
typealias readJsonBlock = (_ response: [AnyObject]) -> Void

class APIManager: NSObject {
    
    static let shared = APIManager()
    
   func Register(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.userLogin.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
   
    func SelfieProofUpload(params: [String: AnyObject]?,profimage : [ImageUpload],callback: @escaping responseCompletionBlock){
        
        
        
        postFormRequest(stringApi: BASEAPI.URL + Base.userLogin.rawValue, parameters: params, images: profimage) { (apiresponse) in
            callback(apiresponse)
        }
    }
    
    private func apiRequest(stringApi: String,method: HTTPMethod, parameters: [String: AnyObject]?,showloading: Bool = true,encoding: ParameterEncoding = URLEncoding.default,header: [String: String] = [:],callback: @escaping responseCompletionBlock){
      
        if(!hasInternet()){
            callback(JSON())
            self.showAlertView()
            return
        }
        
        
        if showloading == true {
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
            SVProgressHUD.setContainerView(topMostViewController().view)
            SVProgressHUD.show()
            
        }
        let myheader = ["X-Authorization":""]
                        //"Authorization":"Bearer "+APPDELEGATE.bearerToken]
        
        print("param \(parameters) stringApi \(stringApi)")
        
        Alamofire.request(stringApi, method: method,parameters: parameters,encoding:encoding,headers: myheader).responseJSON {(response )in
            SVProgressHUD.dismiss()
            do {
                print(JSON(response.data!))
                callback(JSON(response.data!))
            }
            catch {
                
                callback(JSON())
            }
        }
        
    }
    
    // MARK:- POST Image Upload
    private func postFormRequest(stringApi: String, parameters: [String: AnyObject]?,images: [ImageUpload],header: [String: String] = [:], callback: @escaping responseCompletionBlock) {
        
        print("postRequest :",stringApi)
        if(!hasInternet()){
            callback(JSON())
            self.showAlertView()
            return
        }
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
        SVProgressHUD.setContainerView(topMostViewController().view)
        SVProgressHUD.show()
        let URL = try! URLRequest(url: stringApi, method: .post,headers: header)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for imageupload in images {
                let currentTimeStamp = NSDate().timeIntervalSince1970.toString()
                let filename = "\(currentTimeStamp)_img.jpg"
                
                if(imageupload.image.size == .zero){                    
                    multipartFormData.append("".data(using: String.Encoding.utf8)!, withName: "")
                }
                else{
                   
        let datas = imageupload.image.jpegData(compressionQuality: 0.6)
                    if let imageData = imageupload.image.jpegData(compressionQuality: 0.6)  {
                        multipartFormData.append(imageData, withName: imageupload.name, fileName: filename, mimeType: "image/jpeg")
                        
                    }
                }
            }
            
            for (key, value) in parameters! {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
                
            }
        }, with: URL, encodingCompletion: { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    SVProgressHUD.dismiss()
                    print(response.request!)  // original URL request
                    print(response.response!) // URL response
                    print(response.data!)     // server data
                    print(response.result)   // result of response serialization

                    do {
                        let apiResponse = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments)
                        print(apiResponse)
                        callback(JSON(response.data!))
                    }
                    catch {
                        SVProgressHUD.dismiss()
                        callback(JSON())
                    }
                }
                
            case .failure(let encodingError):
                SVProgressHUD.dismiss()
                callback(JSON())
                print(encodingError)
            }
            
        })
    }
    
    func readJson(callback:readJsonBlock) {
        do {
            if let file = Bundle.main.url(forResource: "countries", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: AnyObject] {
                    // json is a dictionary
                    callback([AnyObject]())
                    print(object)
                } else if let object = json as? [AnyObject] {
                    // json is an array
                    print(object)
                    callback(object)
                } else {
                    callback([AnyObject]())
                    print("JSON is invalid")
                }
            } else {
                callback([AnyObject]())
                print("no file")
            }
        } catch {
            callback([AnyObject]())
            print(error.localizedDescription)
        }
    }
    
    func getCoordinates(address: String,callback: @escaping coordinatesCompletionBlock){
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) { (placemarks, error) in
            if(error != nil){
                callback(CLLocationCoordinate2D())
            }
            else{
                if(!(placemarks?.isEmpty)!){
                    callback(placemarks?.first?.location?.coordinate)
                }
                else{
                    callback(CLLocationCoordinate2D())
                }
            }
        }
    }
    
    
}

class ImageUpload{
    var image: UIImage!
    var name: String!
    
    init(uploadimage: UIImage,filename: String){
        image = uploadimage
        name = filename
    }
}

extension Double {
    func format(f: String) -> String {
        return NSString(format: "%\(f)f" as NSString, self) as String
    }
    
    func toString() -> String {
        return String(format: "%f",self)
    }
    
    func toInt() -> Int{
        let temp:Int64 = Int64(self)
        return Int(temp)
    }
}

