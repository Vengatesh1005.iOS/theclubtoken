//
//  ApiList.swift
//  Centros_Camprios
//
//  Created by imac on 12/18/17.
//  Copyright © 2017 Appoets. All rights reserved.
//

import Foundation

//Http Method Types

enum HttpType : String{
    
    case GET = "GET"
    case POST = "POST"
    case PATCH = "PATCH"
    case PUT = "PUT"
    case DELETE = "DELETE"
    
}

// Status Code
struct BASEAPI {
    
    static let AUTHORIZEKEY = ""
    
    static let APIKEY = ""
    
    
    static let DEMOURL = ""
    static let DEMOIMGURL = ""
    
    static let LIVEURL = ""
    static let LIVEIMGURL = ""
    

   
    

    
    static let URL = LIVEURL
    static let IMGURL = LIVEIMGURL
    
    
}



enum StatusCode : Int {
    
    case notreachable = 0
    case success = 200
    case multipleResponse = 300
    case unAuthorized = 401
    case notFound = 404
    case ServerError = 500
    
}



enum Base : String{
    
    case  userLogin = "login"
    case  userRegister = "register"


    
    init(fromRawValue: String){
        self = Base(rawValue: fromRawValue) ?? .userRegister
    }
    
    static func valueFor(Key : String?)->Base{
        
        guard let key = Key else {
            return Base.userRegister
        }
        
        
        if let base = Base(rawValue: key) {
            return base
        }
        
        return Base.userRegister
        
    }
    
}
